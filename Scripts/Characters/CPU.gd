extends "res://Scripts/Characters/Player.gd"

var cpuON = false
var dist = 10000
var target

func AI():
	if cpuON:
		#Exit Shop
		if get_node("../../MapSetup").mode == "Shop":
			target = get_node("../LeaveArea")
			dir = target.translation - self.translation
			
		#Attack players
		else:
			#Find Target
			dist = 10000
			for N in self.get_parent().get_children():
				if N != self: #Don't check self
					if N.is_in_group("Player") && N.alive:
						if dist > self.translation.distance_to(N.translation): #Check distance
							dist = self.translation.distance_to(N.translation)
							target = N
			#Attack
			if target.alive:
				if self.translation.distance_to(target.translation) < 12.5:
					dir = Vector3(0,0,0)
					CPUattack()
				else: #Go after
					dir = target.translation - self.translation
		
	pass

func CPUattack():
	#Punch/Kick
	if Global.canAttack:
		if !isPunching && !isKicking:
			if is_on_floor() && punchTimer.is_stopped():
				isPunching = true
				punchTimer.start()
			elif !is_on_floor() && kickTimer.is_stopped():
				isKicking = true
				kickTimer.start()
	pass

#	#Pick weapon
#	if Input.is_action_just_pressed("joy_pick_"+str(numID)):
#		if itemID != null: #Se está em cima de um item (colidiindo)
#			equipItem()
#	#Jump
#	if Input.is_action_just_pressed("joy_jump_"+str(numID)):
#		if is_on_floor(): 
#			velocity.y += jumpHeight
#		elif bootR != null:
#			if bootR.animName == "JetBoots" && fuel > 0:
#				if isHovering:
#					isHovering = false
#				else:
#					isHovering = true