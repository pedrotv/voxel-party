extends Spatial

var skin
var force = 50

func _ready():
	for x in 6:
		get_node("Part"+str(x+1)).apply_impulse(Vector3(0,0,0),Vector3(randi()%3 - 1,2,randi()%3 - 1) * force)
		get_node("Part"+str(x+1)).set_rotation(Vector3(randi()%360,randi()%360,randi()%360))
		
		var aux = get_node("Part"+str(x+1)+"/Model")
		aux.set_mesh(aux.get_mesh().duplicate())
		var dupe = aux.get_mesh().surface_get_material(0).duplicate()
		aux.get_mesh().surface_set_material(0, dupe)
		aux.mesh.surface_get_material(0).set_texture(0,skin)
	pass
