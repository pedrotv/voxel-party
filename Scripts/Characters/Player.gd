extends KinematicBody

var item = preload("res://Scenes/Weapons/GroundItem.tscn")
var uranium = preload("res://Scenes/Toxic/Uranium.tscn")
var coin = preload("res://Scenes/Objects/Coin.tscn")
var healthBarScene = preload("res://Scenes/Characters/HealthBar.tscn")
var deadPlayer = preload("res://Scenes/Characters/DeadPlayer.tscn")
var dmgPart = preload("res://Scenes/Particles/DmgParticles.tscn")

#Pointers to player nodes
onready var knockTimer = $Timers/KnockbackTimer
onready var frozenTimer = $Timers/FrozenTimer
onready var invTimer = $Timers/InvencibilityTimer
onready var punchTimer = $Timers/PunchTimer
onready var kickTimer = $Timers/KickTimer
onready var hugeTimer = $Timers/HugeTimer
onready var raycast = $RayCast
onready var shadow = $ShadowRef
onready var punchCol = $PunchBody/CollisionShape
onready var kickCol = $KickBody/CollisionShape
onready var jumpSound = $Sound/JumpSound
onready var punchSound = $Sound/PunchSound
onready var freezeSound = $Sound/FreezeSound

var button
var animPlayer
var model

#Ice block instance
var ice

#Weapon IDs
var itemWR = null

#Weapon/boots pointer
var number
var weapon
var bootL
var bootR
var powerup
var bomb
var bombReference

#Skeleton pointer
var skeleton

#Deap player body parts
var corpse = null
var alive = true

#Player States
var canMove = false
var autoWalkForward = false
var invencible = false
var isHovering = false
var isFrozen = false
var isPunching = false
var isGrounded = false
var isKicking = false
var inDeathzone = false
var inLava = false
var inWater = false
var inAcid = false
var isHuge = false
var inLoop = false

#Vector3's
var cam
var knockbackdir
var cameraRot = Vector3(0,0,0)
var dir = Vector3()
var velocity = Vector3()
var initPosition = Vector3()

#Player stats
var speed = 75
var gravity = -400
var jumpHeight = 150
var checkpoint = 0
var fuel = 1
var deadzone = 0.25
var damage = 1
var knockbackForce = 6
var knockbackHit = 0
var blinkingRate = 14
var cameraAngle = 0
var healthBar

#Id of the player
export (int) var numID

func _ready():
	#ice = iceBlock.instance()
	Global.players[numID].collectables = 0
	Global.players[numID].tempCoins = 0
	
	#Instance health bar
	healthBar = healthBarScene.instance()
	get_parent().add_child(healthBar)
	
	#Change mesh
	var playerModel = $PlayerModel
	var m = Global.models[Global.players[numID].playerModel].instance()
	playerModel.add_child(m)
	
	ice = m.get_node("Armature/Skeleton/IceModel")
	skeleton = m.get_node("Armature/Skeleton")
	var aux = m.get_node("Armature/Skeleton/Model")
	aux.set_mesh(aux.get_mesh().duplicate())
	var dupe = aux.get_mesh().surface_get_material(0).duplicate()
	aux.get_mesh().surface_set_material(0, dupe)
	aux.mesh.surface_get_material(0).set_texture(0,Global.players[numID].playerSkin)
	
	animPlayer = $PlayerModel/SceneRoot/AnimationPlayer
	model = $PlayerModel/SceneRoot/Armature/Skeleton/Model
	
	initPosition = self.get_translation()
	
	#Health bar number
	number = healthBar.get_node("Number")
	#Change model
	if Global.players[numID].type == 2:
		number.mesh = Global.typeNumbers[4]
	else:
		number.mesh = Global.typeNumbers[numID]
		
	#Make unique
	number.set_mesh(number.get_mesh().duplicate())
	var dupe2 = number.get_mesh().surface_get_material(0).duplicate()
	number.get_mesh().surface_set_material(0, dupe2)
	
	#Number Color
	if Global.players[numID].team == 0:
		number.mesh.surface_get_material(0).albedo_color = Color(0.25, 0.25, 0.75, 1) #Blue
	elif Global.players[numID].team == 1:
		number.mesh.surface_get_material(0).albedo_color = Color(0.25, 0.75, 0.25, 1) #Green
	elif Global.players[numID].team == 2:
		number.mesh.surface_get_material(0).albedo_color = Color(0.75, 0.25, 0.25, 1) #Red
	elif Global.players[numID].team == 3:
		number.mesh.surface_get_material(0).albedo_color = Color(0.75, 0.75, 0.25, 1) #Yellow
	#Hand icon button
	button = healthBar.get_node("HandIcon")
	
	#Instance bought weapon
	if Global.players[numID].boughtWeapon != null:
		weapon = Global.weapons[Global.players[numID].boughtWeapon].instance()
		skeleton.add_child(weapon)
		Global.players[numID].boughtWeapon = null
	
	#Instance bought boot
	if Global.players[numID].boughtBoot != null:
		bootR = Global.weapons[Global.players[numID].boughtBoot].instance()
		skeleton.add_child(bootR)
		bootL = Global.bootL.instance()
		skeleton.add_child(bootL)
		Global.players[numID].boughtBoot = null
	
	pass

func _physics_process(delta):
	dir = Vector3(0,0,0)
	
	#If is dead, can't move anymore
	if Global.players[numID].life == 0:
		dropMoney()
		#Spawn body parts
		alive = false
		if corpse == null:
			corpse = Global.deadModels[Global.players[numID].playerModel].instance()
			corpse.skin = Global.players[numID].playerSkin
			corpse.set_translation(self.get_translation())
			self.get_parent().add_child(corpse)
			
			#Stop player
			canMove = false
			get_node("PlayerModel").hide()
			$CollisionShape.disabled = true
			gravity = 0
	
	#Control
	movement(delta) #All the movement functions of the player
	manageWeapon(delta) #Control the weapon the player has
	deathzone(delta) #Death zone (kills players instantly)
	collectables() #Manage collectables
	
	#Effects
	shadow() #Players shadow
	healthBar() #Shows health bar
	attack() #Manage player attack
	animations() #Animate
	blinkEffect() #Player blink when he takes dmg
	frezze() #Frezze effect

#Shadow
func shadow():
	if Global.shadow:
		shadow.visible = alive
		#shadow.set_global_rot(initRotation)
		if raycast.is_colliding():
			shadow.set_translation(to_local(raycast.get_collision_point()))
	else:
		shadow.hide()
	pass

#Manage collectables
func collectables():
	#Limit collectables
	if Global.players[numID].collectables > Global.uraniumValue:
		Global.players[numID].collectables = Global.uraniumValue
	if Global.players[numID].collectables < 0:
		Global.players[numID].collectables = 0
	
	#Send collectables
	if Global.players[numID].collectables >= Global.uraniumValue:
		turnHuge()
	if isHuge && Global.players[numID].collectables == 0 && !hugeTimer.is_stopped():
		turnSmall()

#Manage health bar
func healthBar():
	#Show number
	number.visible = Global.showIcons
	
	#Adjust life
	if Global.players[numID].life < 0:
		Global.players[numID].life = 0
	
	#Show if on
	if Global.healthBar:
		healthBar.get_node("Model").show()
		healthBar.get_node("Border").show()
	else:
		healthBar.get_node("Model").hide()
		healthBar.get_node("Border").hide()
		
	#Hide if 0
	if float(Global.players[numID].life)/float(Global.matchClass.lives) == 0:
		healthBar.hide()
		
	#Set scale
	healthBar.get_node("Model").scale = Vector3(1,float(Global.players[numID].life)/float(Global.matchClass.lives),1)
	
	#Health bar follow
	if isHuge:
		healthBar.set_translation(get_translation() + Vector3(0,40,0))
	else:
		healthBar.set_translation(get_translation())

#Control the weapon the player has
func manageWeapon(delta):
	#Rotate pick up button (Y)
	#var b = button.get_rotation()
	#button.set_rotation(Vector3(b.x, b.y + 1 * delta, b.z))
	if itemWR != null:
		button.show()
	else:
		button.hide()
	
	#Make equip weapon follow player movement
	if weapon != null:
		weapon.follow(self)
	if bootL != null:
		bootL.follow(self)
	if bootR != null:
		bootR.follow(self)
	if bombReference != null && bombReference.get_ref() && bomb != null:
		bomb.active = false
		bomb.itemType.start = false
		bomb.itemType.follow(bomb,self)

#All the movement functions, for both keyboard and joypad
func movement(delta):
	#If can move
	if canMove:
		if Global.players[numID].type == 1:
			#Move with arrows/WASD
			if Global.players[numID].deviceID == 5:
				keyboardMove()
				
			#Movement with joypad
			elif Global.players[numID].deviceID == numID:
				joypadMove()
		elif Global.players[numID].type == 2:
			AI()
	
	#Walk forward automaticaly
	if autoWalkForward:
		dir += -cam.basis[2]
	
	#Angle
	if dir.x != 0 || dir.z != 0:
		var angle = Vector2(dir.x,dir.z)
		angle = angle.angle()
		self.rotation_degrees.y = -rad2deg(angle) + 90
	
	#Normalize (walk normaly on diagonal)
	if knockTimer.get_time_left() == 0:
		dir = dir.normalized() * speed
	else:
		dir = knockbackdir.normalized() * speed * (knockbackHit/2)
	velocity.x = dir.x
	velocity.z = dir.z
	
	#Normal gravity
	velocity.y += gravity * delta
	
	#Hover boots
	if isHovering:
		fuel -= 1*delta
		velocity.y = 0
		if fuel <= 0:
			isHovering = false
	if bootL != null:
		bootL.get_node("MeshInstance/Particles").emitting = isHovering
	if bootR != null:
		bootR.get_node("MeshInstance/Particles").emitting = isHovering
	
	#Recharge fel
	if is_on_floor():
		fuel = 1
		if isHuge:
			speed = 60
		else:
			speed = 75
	
	#Make movement
	velocity = move_and_slide(velocity, Vector3(0,1,0))
	
	#Grab item
	itemCol()
	
	#Check punch/kick collision
	punchKickCol($PunchBody)
	punchKickCol($KickBody)
	
	pass

#Move through keyboard
func keyboardMove():
	if Input.is_action_pressed("ui_move_left"):
		dir += -cam.basis[0]
	if Input.is_action_pressed("ui_move_right"):
		dir += cam.basis[0]
	if Input.is_action_pressed("ui_move_up"):
		dir += -cam.basis[2]
	if Input.is_action_pressed("ui_move_down"):
		dir += cam.basis[2]
	
	#Camera Zoom
	if Input.is_action_pressed("ui_zoom_out"):
		cameraAngle = -1
	elif Input.is_action_pressed("ui_zoom_in"):
		cameraAngle = 1
	else:
		cameraAngle = 0
	
	#Camera Rot
	if Input.is_action_pressed("ui_rotate_left"):
		cameraRot.y += -1
	elif Input.is_action_pressed("ui_rotate_right"):
		cameraRot.y += 1
	
	#Reset
	if Input.is_joy_button_pressed(numID, 9):
		cameraRot = Vector3(0,0,0)
	
	#Limits
	var limit = 25
	if cameraRot.x <= -limit:
		cameraRot.x = -limit
	if cameraRot.x >= limit:
		cameraRot.x = limit
	
	if Input.is_action_just_pressed("ui_show_icons"):
		Global.showIcons = !Global.showIcons
	
	#Pick weapon
	if Input.is_action_just_pressed("ui_pick_weapon"):
		if itemWR != null:
			equipItem()
	
	#Jump
	if Input.is_action_just_pressed("ui_jump"):
		if is_on_floor():
			velocity.y += jumpHeight
			Global.sfx3DSound(jumpSound)
			jumpSound.play()
		elif bootR != null:
			if bootR.animName == "JetBoots" && fuel > 0:
				if isHovering:
					isHovering = false
				else:
					isHovering = true
	
	#Punch
	if Global.canAttack:
		if !isPunching && !isKicking:
			if Input.is_action_just_pressed("ui_attack"):
				if is_on_floor() && punchTimer.is_stopped():
					isPunching = true
					punchTimer.start()
				elif !is_on_floor() && kickTimer.is_stopped():
					isKicking = true
					kickTimer.start()

#Move through joypad
func joypadMove():
	#Joypad movement
	dir += cam.basis[0] * (-Input.get_action_strength("joy_left_"+str(numID)) + Input.get_action_strength("joy_right_"+str(numID)))
	dir += cam.basis[2] * (+Input.get_action_strength("joy_down_"+str(numID)) - Input.get_action_strength("joy_up_"+str(numID)))
	
	#D-PAD
	if Input.is_action_pressed("joy_pad_left_"+str(numID)):
		dir += -cam.basis[0]
	if Input.is_action_pressed("joy_pad_right_"+str(numID)):
		dir += cam.basis[0]
	if Input.is_action_pressed("joy_pad_up_"+str(numID)):
		dir += -cam.basis[2]
	if Input.is_action_pressed("joy_pad_down_"+str(numID)):
		dir += cam.basis[2]
	
	#Camera Zoom
	if Input.is_action_pressed("joy_zoom_out_"+str(numID)):
		cameraAngle = -1
	elif Input.is_action_pressed("joy_zoom_in_"+str(numID)):
		cameraAngle = 1
	else:
		cameraAngle = 0
	
	#Camera Rot
	if !(Input.get_joy_axis(numID, 3) <= deadzone && Input.get_joy_axis(numID, 3) >= -deadzone):
		cameraRot.x += Input.get_joy_axis(numID, 3)
	if !(Input.get_joy_axis(numID, 2) <= deadzone && Input.get_joy_axis(numID, 2) >= -deadzone):
		cameraRot.y += Input.get_joy_axis(numID, 2)
	
	#Reset
	if Input.is_joy_button_pressed(numID, 9):
		cameraRot = Vector3(0,0,0)
	
	#Limits
	var limit = 25
	if cameraRot.x <= -limit:
		cameraRot.x = -limit
	if cameraRot.x >= limit:
		cameraRot.x = limit
	
	if Input.is_action_just_pressed("joy_select_"+str(numID)):
		Global.showIcons = !Global.showIcons
	
	#Pick weapon
	if Input.is_action_just_pressed("joy_pick_"+str(numID)):
		if itemWR != null: #Se está em cima de um item (colidiindo)
			equipItem()
	
	#Jump
	if Input.is_action_just_pressed("joy_jump_"+str(numID)):
		if is_on_floor(): 
			velocity.y += jumpHeight
			Global.sfx3DSound(jumpSound)
			jumpSound.play()
		elif bootR != null:
			if bootR.animName == "JetBoots" && fuel > 0:
				if isHovering:
					isHovering = false
				else:
					isHovering = true
	
	#Punch/Kick
	if Global.canAttack:
		if !isPunching && !isKicking:
			if Input.is_action_just_pressed("joy_attack_"+str(numID)):
				if is_on_floor() && punchTimer.is_stopped():
					isPunching = true
					punchTimer.start()
				elif !is_on_floor() && kickTimer.is_stopped():
					isKicking = true
					kickTimer.start()

#If player is on deathzone, kills him
func deathzone(delta):
	if inDeathzone:
		if inLava:
			Global.players[numID].life = 0
			velocity = Vector3(0,0,0) 
			gravity = -25 #Fall slowly
			velocity.y += gravity * delta
			velocity = move_and_slide(velocity, Vector3(0,1,0))
		elif inWater:
			self.set_translation(initPosition+Vector3(0,150,0)) 
			enviromentDamage(1)
			inWater = false
			inDeathzone = false
			speed = 0
		elif inAcid:
			if !isHuge:
				enviromentDamage(3)
		elif inLoop:
			self.set_translation(initPosition+Vector3(0,150,0)) 
			inLoop = false
			inDeathzone = false
			speed = 0
		else:
			Global.players[numID].life = 0 #Player is dead
	pass

#Manage player attack
func attack():
	#Punch
	if isPunching:
		#If has a bomb
		if bombReference != null && bombReference.get_ref():
			bomb.itemType.weaponEffect(self.get_parent(),self)
			bombReference = null
			bomb = null
		else:
			punchCol.disabled = false
			#If the weapon has a effect
			if weapon != null:
				weapon.weaponEffect(self.get_parent(),self)
			else:
				Global.sfx3DSound(punchSound)
				punchSound.play()
	else:
		punchCol.disabled = true
	
	#Kick
	if isKicking:
		#If has a bomb
		if bombReference != null && bombReference.get_ref():
			bomb.itemType.weaponEffect(self.get_parent(),self)
			bombReference = null
			bomb = null
		else:
			kickCol.disabled = false
			#If the boot has a effect
			if bootR != null:
				bootR.weaponEffect(self.get_parent(),self)
	else:
		kickCol.disabled = true
	pass

#Manages the animation player
func animations():
	if isFrozen:
		animPlayer.stop()
	else:
		if !is_on_floor():
		#if !isGrounded:
			if isKicking:
				animPlayer.play("Kick",0.1, 5)
			else:
				animPlayer.play("Jump",0.01,0.1)
		elif isPunching:
			if weapon != null:
				animPlayer.play(weapon.animName,0.1, 5)
			else:
				animPlayer.play("Punch",0.1, 5)
		elif dir.x != 0 || dir.z != 0:
			animPlayer.play("Movement",0.1,3.5)
		else:
			animPlayer.play("Idle",0.1,1)
	pass

#Blink effect
func blinkEffect():
	if invencible:
		if Engine.get_frames_drawn() % blinkingRate < blinkingRate/2:
			model.get_mesh().surface_get_material(0).albedo_color = Color(0.5,0,0,1) 
		else:
			model.get_mesh().surface_get_material(0).albedo_color = Color(1,1,1,1)
	pass

#Check if colliding with item
func itemCol():
	var areaCollsion = $Area.get_overlapping_bodies()
	if areaCollsion.size() > 0:
		for N in areaCollsion.size():
			if areaCollsion[N].is_in_group("Item") && areaCollsion[N].is_on_floor():
				itemWR = areaCollsion[N]
				#itemRef = areaCollsion[N]
				
				#Auto equip
				if !itemWR.shop:
					if itemWR.itemType.typeName == "Weapon" && weapon == null || itemWR.itemType.typeName == "Boot" && bootL == null || itemWR.itemType.typeName == "Powerup" || itemWR.itemType.typeName == "Uranium" || itemWR.itemType.typeName == "Coin" || itemWR.itemType.typeName == "SackOfCoins":
						equipItem()

#If hit player with a punch/kick
func punchKickCol(var p):
	var punchCollsion = p.get_overlapping_bodies()
	if punchCollsion.size() > 0:
		for N in punchCollsion.size():
			if (punchCollsion[N].is_in_group("Player") && punchCollsion[N] != self) || punchCollsion[N].is_in_group("Crate") || punchCollsion[N].is_in_group("InteractableObj"):
				if weapon != null && isPunching:
					punchCollsion[N].takeDamage(self,damage + weapon.damage,knockbackForce + weapon.knockbackForce)
				else:
					punchCollsion[N].takeDamage(self,damage,knockbackForce)

#Do dmg, apply knockback and turn on invencibility
func takeDamage(attacker, damage, knockback):
	if invTimer.get_time_left() == 0 && Global.players[numID].life > 0:
		if !(attacker != null && isHuge && !attacker.isHuge):
			if damage > 0:
				Global.players[numID].life -= damage
				knockbackHit = knockback
				invencible = true #Turn on invencibility
				invTimer.start() #Turn on invencibilty timer
				knockTimer.start() #Turn on knockback timer
				if attacker != null:
					var s = self.get_global_transform().origin
					var a = attacker.get_global_transform().origin
					s = Vector3(s.x, 0, s.z)
					a = Vector3(a.x, 0, a.z)
					knockbackdir = s - a
				if Global.rumble:
					if Global.players[numID].type == 1:
						Input.start_joy_vibration(numID,1,0,invTimer.wait_time)
				velocity.y += jumpHeight/3
				
				#Unfreeze
				if isFrozen:
					unfreeze()
				
				#Drop collectables (collect type maps)
				dropCollectables()
				dropMoney()
				#dmgParticles()
				
	pass

func enviromentDamage(damage):
	if invTimer.get_time_left() == 0 && Global.players[numID].life > 0:
		if damage > 0:
			Global.players[numID].life -= damage
			invencible = true #Turn on invencibility
			invTimer.start() #Turn on invencibilty timer
			if Global.rumble:
				if Global.players[numID].type == 1:
					Input.start_joy_vibration(numID,1,0,invTimer.wait_time)
			velocity.y += jumpHeight/3
			
			#Unfreeze
			if isFrozen:
				unfreeze()
			
			#Drop collectables (collect type maps)
			dropCollectables()
			dropMoney()

func dmgParticles():
	var w = dmgPart.instance()
	w.dir = knockbackdir
	w.set_translation(self.get_translation() + Vector3(0,15,0))
	self.get_parent().add_child(w)

#Drop collectables (collect type maps)
func dropCollectables():
	if !isHuge:
		if Global.players[numID].collectables > 0:
			var drops = randi()%3 + 3 #Can drop between 3 to 5 itens
			if drops > Global.players[numID].collectables:
				drops = Global.players[numID].collectables
			Global.players[numID].collectables -= drops
			
			for x in drops:
				#Instance uranium
				var w = item.instance()
				w.itemType = uranium.instance()
				w.set_translation(self.get_translation() + Vector3(0,15,0))
				self.get_parent().add_child(w)

#Drop money
func dropMoney(all = false):
	if Global.players[numID].tempCoins > 0:
		var drops = (randi()%3 + 3)*100 #Can drop between 3 to 5 itens
		if drops > Global.players[numID].tempCoins:
			drops = Global.players[numID].tempCoins
		if all:
			drops = Global.players[numID].tempCoins
		Global.players[numID].tempCoins -= drops
		
		drops = drops/100
		for x in drops:
			#Instance uranium
			var w = item.instance()
			w.itemType = coin.instance()
			w.set_translation(self.get_translation() + Vector3(0,15,0))
			self.get_parent().add_child(w)

#Frezze effect
func frezze():
	if isFrozen && frozenTimer.is_stopped():
		frozenTimer.start()
		canMove = false
		ice.show()
		Global.sfx3DSound(freezeSound)
		freezeSound.play()

#Unfreeze
func unfreeze():
	isFrozen = false
	canMove = true
	ice.hide()

#Equip a new time
func equipItem():
	if itemWR.shop:
		if Global.players[numID].coins >= itemWR.shopPrice:
			Global.players[numID].coins -= itemWR.shopPrice
		else:
			return
	
	#if it's a weapon:
	if itemWR.itemType.typeName == "Weapon":
		#Remove previous equiped weapon
		if weapon != null:
			weapon.queue_free()
		#Instance item
		weapon = Global.weapons[itemWR.id].instance()
		skeleton.add_child(weapon)
		if itemWR.shop:
			Global.players[numID].boughtWeapon = itemWR.id
	#if it's a boot
	elif itemWR.itemType.typeName == "Boot":
		#Remove previous boot
		if bootL != null:
			bootL.queue_free()
		if bootR != null:
			bootR.queue_free()
		#Instance item
		bootR = Global.weapons[itemWR.id].instance()
		skeleton.add_child(bootR)
		#Other boot
		bootL = Global.bootL.instance()
		skeleton.add_child(bootL)
		#---
		if itemWR.shop:
			Global.players[numID].boughtBoot = itemWR.id
	#if it's a bomb:
	elif itemWR.itemType.typeName == "Bomb" && !isHuge:
		if bombReference == null || !bombReference.get_ref():
			#Remove previous equiped weapon
			bombReference = weakref(itemWR)
			bomb = null
			#Instance item
			bomb = itemWR
			bomb.get_parent().remove_child(bomb)
			skeleton.add_child(bomb)
	#If it's a powerup
	elif itemWR.itemType.typeName == "Powerup":
		powerup = Global.weapons[itemWR.id].instance()
		self.add_child(powerup)
	#If it's a powerup
	elif itemWR.itemType.typeName == "Uranium":
		Global.players[numID].collectables += 1
	#If it's coins
	elif itemWR.itemType.typeName == "Coin":
		Global.players[numID].tempCoins += 100
	#If it's sack of coins
	elif itemWR.itemType.typeName == "SackOfCoins":
		Global.players[numID].tempCoins += 500
	
	#Remove weapon from map (Unless it's bomb or shop item)
	if itemWR != null:
		if !itemWR.shop:
			itemWR.queue_free()
	pass

#Ended invencibility
func _on_InvencibilityTimer_timeout():
	invencible = false #Turn off invencibilty
	model.get_mesh().surface_get_material(0).albedo_color = Color(1,1,1,1) #Stop blinking
	pass

#Become a giant
func turnHuge(forever = false):
	if !isHuge:
		isHuge = true
		#collectables = 0
		self.scale = Vector3(2.5,2.5,2.5)
		damage = 5
		knockbackForce = 10
		speed = 60
		if !forever:
			hugeTimer.start()

#If player stopped touching (exit)
func _on_Area_body_shape_exited(body_id, body, body_shape, area_shape):
	if body.is_in_group("Item"):
		itemWR = null
	pass

#Ended timer of attack
func _on_AttackTimer_timeout():
	isPunching = false
	isKicking = false
	pass 

#Freeze timeout
func _on_FrozenTimer_timeout():
	unfreeze()
	pass

#Go small
func _on_HugeTimer_timeout():
	Global.players[numID].collectables -= 1
	pass

func turnSmall():
	hugeTimer.stop()
	isHuge = false
	self.scale = Vector3(1,1,1)
	damage = 1
	knockbackForce = 6
	speed = 75

func AI():
	pass