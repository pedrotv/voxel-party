extends Spatial

var traps = []

func _ready():
#if !$MapSetup.preview:
	traps.resize(5)
	traps[0] = load("res://Scenes/RaceSegments/FloatCrateSpawners.tscn")
	traps[1] = load("res://Scenes/RaceSegments/FallingRocks.tscn")
	traps[2] = load("res://Scenes/RaceSegments/FallingTrunks.tscn")
	traps[3] = load("res://Scenes/RaceSegments/MovingTrunks.tscn")
	traps[4] = load("res://Scenes/RaceSegments/RotatingRocks.tscn")
	#traps[5] = load("res://Scenes/RaceSegments/RotatingLog.tscn")
	
	for x in 3:
		generateTrap(-140 + (-400 * x))
	pass

func generateTrap(pos):
	var r = randi()%traps.size()
	var t = traps[r].instance()
	$Map.add_child(t)
	t.translation.z += pos
	traps.remove(r)
	pass