extends Spatial

onready var toxic = $Toxic
onready var spawnTimer = $SpawnerTimer
onready var mapSetup = $MapSetup

#var crate = load("res://Scenes/Toxic/ToxicContainer.tscn")
#var numOfCrates = 0
#var turns = 1
#var maxCrates = 5

func _on_SpawnerTimer_timeout():
	if !mapSetup.preview:
		var numOfSpawns = (round(turns/3)+1) #Number of crates that will be spawned (depends on the turn)		
		#print(str(numOfSpawns)+"/"+str(numOfCrates))
		if numOfCrates + numOfSpawns <= maxCrates:
			for aux in numOfSpawns:
				spawnCrate()
			turns += 1
	pass

func spawnCrate():
	var c = crate.instance()
	c.mapNode = self
	toxic.add_child(c)
	var rx = randi()%200 - 100
	var rz = randi()%120 - 20
	c.set_translation(Vector3(rx,45,rz))
	numOfCrates += 1
	