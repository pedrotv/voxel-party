extends Spatial

var test = load("res://Scenes/Test/TestChars.tscn")

func _ready():
	for x in 4:
		Global.players[x].slot = x
		Global.players[x].playerModel = x
		Global.players[x].playerSkin = load("res://Materials/Hero/images/Hero1.png")
		if Global.players[x].deviceID != null:
			Global.players[x].type = 1
		else:
			Global.players[x].type = 2
	
	var t = test.instance()
	$MapSetup.add_child(t)
	
	Global.fade.fadeIn()
	pass
