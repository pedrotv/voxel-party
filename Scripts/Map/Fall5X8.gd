extends Spatial

var blueSpace = load("res://Models/ObjType/Tiles/ColoredTiles/ClearBlueSpace.obj")
var redSpace = load("res://Models/ObjType/Tiles/ColoredTiles/RedSpace.obj")
var skullSpace = load("res://Models/ObjType/Tiles/ColoredTiles/SkullSpace.obj")

onready var floorTimer = $FloorTimer
onready var floordownTimer = $FloordownTimer
onready var countdown = $MapSetup/Countdown
onready var map = $Map
onready var mapSetup = $MapSetup

var plataforms = []
var fall = []

var blinkingRate = 20
var r
var turns = 0
var dupe

func _ready():
	var M = 0
	plataforms.resize(map.get_child_count())
	for N in map.get_children():
		if N.get_child_count() > 0:
			plataforms[M] = N
			dupe = plataforms[M].get_node("Model").get_mesh().surface_get_material(0).duplicate() #Dupe material
			M += 1
	pass

func generateFloor(x):
	var r = randi()%plataforms.size()
	fall[x] = plataforms[r]
	plataforms.remove(r)

func startFloorFall(numSpaces):
	#Start timer for floor
	if !countdown.visible:
		if plataforms.size() > numSpaces:
			#Select a random space
			fall.resize(numSpaces)
			for x in numSpaces:
				generateFloor(x)
				
			#Increase turns, start timer for floor falling and restore other spaces
			turns += 1
			floorTimer.start()
	pass

func _physics_process(delta):
	if !countdown.visible && turns == 0:
		 startFloorFall(4)
	
	#Blink effect
	if floorTimer.get_time_left() > 0:
		for x in fall.size():
			#Falling plataform
			if Engine.get_frames_drawn() % blinkingRate < blinkingRate/2:
				fall[x].get_node("Model").set_mesh(redSpace) 
				fall[x].get_node("Model").get_mesh().surface_set_material(0, dupe) #Set the duped mesh
			else:
				fall[x].get_node("Model").set_mesh(blueSpace)
				fall[x].get_node("Model").get_mesh().surface_set_material(0, dupe) #Set the duped mesh
	
	#Tile falling
	if floordownTimer.get_time_left() > 0:
		var velocity = Vector3(0,0,0)
		velocity.y += (-3000) * delta
		for x in fall.size():
			if fall[x] != null:
				velocity = fall[x].move_and_slide(velocity, Vector3(0,1,0))
	
	pass

func _on_FloorTimer_timeout():
	#Change mesh to skull and start falling timer
	for x in fall.size():
		fall[x].get_node("Model").set_mesh(skullSpace)
		fall[x].get_node("Model").get_mesh().surface_set_material(0, dupe) #Set the duped mesh
	floordownTimer.start()
	pass

func _on_FloordownTimer_timeout():
	#Disable/hide falling plataforms
	for x in fall.size():
		fall[x].queue_free()
	fall.clear()
	
	var num = 4
	#Set number of falling plataforms
	if turns >= 9:
		floorTimer.set_wait_time(1.3)
		floordownTimer.set_wait_time(1.25)
		startFloorFall(3 + num)
	elif turns >= 6:
		floorTimer.set_wait_time(1.7)
		floordownTimer.set_wait_time(1.5)
		startFloorFall(2 + num)
	elif turns >= 3:
		floorTimer.set_wait_time(2.1)
		floordownTimer.set_wait_time(1.75)
		startFloorFall(1 + num)
	else:
		startFloorFall(num)
	pass