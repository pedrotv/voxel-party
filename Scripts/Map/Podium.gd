extends Spatial

onready var camera = $Camera
onready var topCamera = $Viewport/CameraTop
onready var anim = $AnimationPlayer
onready var winBanner = $WinnerText
onready var winLabel = $WinnerText/Label
onready var winLabelShadows = $WinnerText/LabelShadow

var playerScene = preload("res://Scenes/Characters/Player.tscn")
var cpuScene = preload("res://Scenes/Characters/CPU.tscn")

#UI containing player name and hearts
var podiuns = []
var allPlayers = []
var preview = false
var matchOn = true
var winners = []
var wins = []

export (int) var cameraMode = 1
export (String) var mode

func _ready():
	Global.camera = cameraMode
	
	podiuns.resize(4)
	
	Global.music.stop()
	
	for x in 4:
		podiuns[x] = load("res://Scenes/Podiums/Podium"+str(x+1)+".tscn")
		
		#Set lives
		Global.players[x].life = int(Global.matchClass.lives)
		
		#Remove character not being used
		if Global.players[x].type == 0:
			Global.players[x].coins == -1
			get_node("Profiles/PlayerProfile"+str(x)).queue_free()
		elif Global.players[x].type == 1:
			var p = playerScene.instance()
			p.numID = x
			p.set_global_transform(get_node("P/P"+str(x+1)).get_global_transform())
			allPlayers.append(p)
			self.add_child(p)
			p.set_translation(Vector3(-60 + 40*x,90,-145))
		else:
			var p = cpuScene.instance()
			p.numID = x
			p.set_global_transform(get_node("P/P"+str(x+1)).get_global_transform())
			allPlayers.append(p)
			self.add_child(p)
			p.set_translation(Vector3(-60 + 40*x,90,-145))
	
	get_node("P").queue_free()
	
	#Start splitscreen
	$SplitScreen.initiate()
	
	#Set wins array
	if Global.gameModeType == 1:
		$TrophyProfiles.show()
		for x in 4:
			if !wins.has(Global.players[x].wins):
				wins.append(Global.players[x].wins)
		wins.sort()
		wins.invert()
		print(wins)
		
		#Instance podiuns
		for x in wins.size():
			for y in 4:
				if wins[x] == Global.players[y].wins: #Se o player é o proximo na lista
					if Global.players[y].type != 0 && Global.players[y].position == 5: #Se ele for bot ou player
						Global.players[y].position = x
						var p = podiuns[x].instance()
						get_node("../Map/p"+str(y+1)).add_child(p)
						if x == 0:
							for n in allPlayers.size():
								if allPlayers[n].numID == y:
									allPlayers[n].turnHuge(true)
							winners.append(y)
	else:
		$CoinProfiles.show()
		for x in 4:
			if !wins.has(Global.players[x].coins):
				wins.append(Global.players[x].coins)
		wins.sort()
		wins.invert()
		print(wins)
		
		#Instance podiuns
		for x in wins.size():
			for y in 4:
				if wins[x] == Global.players[y].coins: #Se o player é o proximo na lista
					if Global.players[y].type != 0 && Global.players[y].position == 5: #Se ele for bot ou player
						Global.players[y].position = x
						var p = podiuns[x].instance()
						get_node("../Map/p"+str(y+1)).add_child(p)
						if x == 0:
							for n in allPlayers.size():
								if allPlayers[n].numID == y:
									allPlayers[n].turnHuge(true)
							winners.append(y)
	
	if winners.size() == 1: #Winner
		$Musics/Win.play()
	else: #Draw
		$Musics/Draw.play()
	
	#Set animation
	$AnimationPlayer.play("CameraAnim",0,1)

func _physics_process(delta):
	Global.musicSound($Musics/Win)
	Global.musicSound($Musics/Draw)
	
	if Input.is_action_just_pressed("ui_pause"):
		Global.uiSound.play()
		Global.fade.fadeOut("res://Scenes/Menus/MainMenu.tscn")

func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "CameraAnim":
		$AnimationPlayer.stop()
		for x in allPlayers.size():
			allPlayers[x].canMove = true
		
		winBanner.show()
		if winners.size() > 1:
			winLabel.text = "Draw!!!"
		else:
			winLabel.text = "Player "+str(winners[0]+1)+ " is the winner!!!"
		winLabelShadows.text = winLabel.text
	pass