extends Spatial

var playerScene = preload("res://Scenes/Characters/Player.tscn")
var cpuScene = preload("res://Scenes/Characters/CPU.tscn")

#Timer of the map
onready var matchTimer = $MatchTimer
onready var playerAliveTimer = $PlayerAliveTimer
onready var timerLabel = $Timer/TimerLabel
onready var timerLabel2 = $Timer/TimerLabel2

onready var camera = $Camera
onready var topCamera = $Viewport/CameraTop

#UI containig player name and hearts
var allPlayers = []
var winner = -1
var matchOn = true
var selectedMap

export (String) var mode
export (int) var cameraMode = 1
export (bool) var canAttack = true

func _ready():
	Global.camera = cameraMode
	Global.canAttack = canAttack
	
	#Set timer
	$Timer.show()
	#matchTimer.wait_time = 300
	matchTimer.start()
	timerLabel.text = str(int(matchTimer.wait_time)/60) + ":" + str(int(matchTimer.wait_time)%60)
	if int(matchTimer.wait_time)%60 == 0:
		timerLabel.text = timerLabel.text + "0"
	timerLabel2.text = timerLabel.text
	
	for x in 4:
		#Set lives
		Global.players[x].life = int(Global.matchClass.lives)
		
		#Show money
		if Global.players[x].type == 0:
			get_node("CoinsProfileGameshow/CoinsProfile"+str(x+1)).queue_free()
		
		#Remove character not being used
		if Global.players[x].type == 1:
			var p = playerScene.instance()
			p.numID = x
			p.set_global_transform(get_node("P/P"+str(x+1)).get_global_transform())
			p.canMove = true
			allPlayers.append(p)
			self.add_child(p)
		elif Global.players[x].type == 2:
			var p = cpuScene.instance()
			p.numID = x
			p.set_global_transform(get_node("P/P"+str(x+1)).get_global_transform())
			p.canMove = true
			allPlayers.append(p)
			self.add_child(p)
	
	get_node("P").queue_free()
	
	#Start splitscreen
	$SplitScreen.initiate()
	
	#Select map
	if Global.specialMap == 0:
		selectedMap = randi()%Global.maps.size()
		get_node("/root/Shop/Map/BackWall/TV/Sprite3D").texture = Global.mapPreviews[selectedMap]
	elif Global.specialMap == 1:
		selectedMap = randi()%Global.coinMaps.size()
		get_node("/root/Shop/Map/BackWall/TV/Sprite3D").texture = Global.coinMapPreviews[selectedMap]
	
	pass 

func _physics_process(delta):
	#Set Timer text
	if !matchTimer.is_stopped():
		timerLabel.text = str(int(matchTimer.get_time_left())/60) + ":" + str(int(matchTimer.get_time_left())%60)
		if int(matchTimer.get_time_left())%60 < 10:
			timerLabel.text = str(int(matchTimer.get_time_left())/60) + ":0" + str(int(matchTimer.get_time_left())%60)
		timerLabel2.text = timerLabel.text
	
	#Game mode
	gameModeRules()
	
	pass

#Timer run out (declare tie)
func _on_MatchTimer_timeout():
	gameModeRules(true)
	get_node("../Map/AnimationPlayer").play("Fall")
	pass

#Rules for the game mode
func gameModeRules(tie = false):
	if matchOn:
		var alivePlayers = 0
		
		#Check which players are alive
		for x in 4:
			if Global.players[x].type != 0:
				if Global.players[x].life > 0:
					alivePlayers += 1
		
		#If the game was a tie
		if tie: 
			finishMatch()
		
		#If player won the race
		elif winner >= 0:
			finishMatch()
			Global.players[winner].wins += 1
		
		#If only one player was alive
		elif alivePlayers <= 1:
			if playerAliveTimer.is_stopped():
				playerAliveTimer.start()
		
	pass

func finishMatch():
	matchTimer.set_paused(true)
	matchOn = false
	$ChangeSceneTimer.start()

func _on_FinishLine_body_shape_entered(body_id, body, body_shape, area_shape):
	if (body.is_in_group("Player")):
		winner = body.numID
	pass

func _on_PlayerAliveTimer_timeout():
	var alivePlayers = 0
	
	#Check which players are alive
	for x in 4:
		if Global.players[x].type != 0:
			if Global.players[x].life > 0:
				alivePlayers += 1
	
	if alivePlayers == 1:
		finishMatch()
		for x in 4:
			if Global.players[x].type != 0 && Global.players[x].life > 0:
				Global.players[x].wins += 1
	else:
		finishMatch()
	pass

func _on_ChangeSceneTimer_timeout():
	if Global.specialMap == 0:
		Global.fade.fadeOutNumber(Global.maps[selectedMap])
	elif Global.specialMap == 1:
		Global.fade.fadeOutNumber(Global.coinMaps[selectedMap])
	
	Global.specialMap = 0
	pass