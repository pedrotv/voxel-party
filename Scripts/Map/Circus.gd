extends Spatial

onready var timer = $Gear/PunchTarget/Timer
onready var mainPlataform = $MainPlataform
onready var gear = $Gear
onready var drill = $Drill
onready var anim = $Drill/AnimationPlayer
onready var punchSign = $Sign

var rot_speed = rad2deg(0.003)
var mainPlatSpeed = 1
var gearSpeed = 0
var dir = 1
var drillUp = false

func _physics_process(delta):
	#Fix direction
	dir = $Sign.dir
	
	#Rotate gear
	var aux = gear.get_rotation()
	gear.set_rotation(Vector3(aux.x, aux.y, aux.z + (rot_speed * gearSpeed * dir) * delta))
	
	#Main platafrom rotation
	mainPlatSpeed = $Gear/PunchTarget.speed
	aux = mainPlataform.get_rotation()
	mainPlataform.set_rotation(Vector3(aux.x, aux.y + (-rot_speed * mainPlatSpeed * dir) * delta, aux.z))
	
	#Coins
	if mainPlatSpeed == 1:
		$CoinSpawner.coinNum = 1
	elif mainPlatSpeed > 1 && mainPlatSpeed < 4:
		$CoinSpawner.coinNum = 2
	else:
		$CoinSpawner.coinNum = 3
	
	#If timer is on, gear was activated, so give it speed
	if !timer.is_stopped():
		gearSpeed = 50
		if !drillUp:
			anim.play("Rotate")
			drillUp = true
	
	#Timer stopped: Make gear lose speed
	if timer.is_stopped() && gearSpeed > 0:
		gearSpeed -= 0.5
		if gearSpeed < 0:
			gearSpeed = 0
	
	#Make drill fall down
	if drillUp:
		var areaCollsion = $Drill/Area.get_overlapping_bodies()
		if areaCollsion.size() > 0:
			for N in areaCollsion.size():
				if areaCollsion[N].is_in_group("Player"):
					anim.play_backwards("Rotate")
					drillUp = false
	else:
		$Gear/PunchTarget.speed -= 0.25
	
	pass

func _on_AnimationPlayer_animation_finished(anim_name):
	anim.stop()
	pass