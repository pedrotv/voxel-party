  extends Spatial

var blueSpace = load("res://Models/ObjType/Tiles/ColoredTiles/ClearBlueSpace.obj")
var redSpace = load("res://Models/ObjType/Tiles/ColoredTiles/RedSpace.obj")
var skullSpace = load("res://Models/ObjType/Tiles/ColoredTiles/SkullSpace.obj")

onready var floorTimer = $FloorTimer
onready var floordownTimer = $FloordownTimer
onready var restoreTimer = $RestoreTimer
onready var countdown = $MapSetup/Countdown
onready var map = $Map
onready var mapSetup = $MapSetup

var plataforms = []
var restore = []
var fall = []

var blinkingRate = 20
var r
var turns = 0
var dupe

func _ready():
	var M = 0
	plataforms.resize(map.get_child_count())
	for N in map.get_children():
		if N.get_child_count() > 0:
			plataforms[M] = N
			dupe = plataforms[M].get_node("Model").get_mesh().surface_get_material(0).duplicate() #Dupe material
			M += 1
	restore.resize(plataforms.size())
	pass

func generateFallSpace():
	var r = randi()%9
	
	if fall.has(r):
		generateFallSpace()
	else:
		fall.append(r)
	pass

func startFloorFall():
	var numSpaces = turns + 1
	if numSpaces > 8:
		numSpaces = 8
	
	#Start timer for floor
	fall.clear()
	
	for x in numSpaces:
		generateFallSpace()
	
	#Increase turns, start timer for floor falling and restore other spaces
	turns += 1
	floorTimer.start()
	pass

func _process(delta):
	if !countdown.visible && turns == 0:
		 startFloorFall()
	
	#Blink effect
	if floorTimer.get_time_left() > 0:
		for x in fall.size():
			if fall[x] != null:
				#Falling plataform
				if Engine.get_frames_drawn() % blinkingRate < blinkingRate/2:
					plataforms[fall[x]].get_node("Model").set_mesh(redSpace) 
					plataforms[fall[x]].get_node("Model").get_mesh().surface_set_material(0, dupe) #Set the duped mesh
				else:
					plataforms[fall[x]].get_node("Model").set_mesh(blueSpace)
					plataforms[fall[x]].get_node("Model").get_mesh().surface_set_material(0, dupe) #Set the duped mesh
	
	#Restoring plataform
	if restoreTimer.get_time_left() > 0:
		for x in restore.size():
			if restore[x] != null:
				plataforms[restore[x]].get_node("Model").show()
				plataforms[restore[x]].get_node("CollisionShape").disabled = false
				var s = plataforms[restore[x]].get_scale()
				var speed = (2) * delta
				plataforms[restore[x]].set_scale(Vector3(s.x + speed,s.y + speed,s.z + speed))
	
	#Tile falling
	if floordownTimer.get_time_left() > 0:
		var velocity = Vector3(0,0,0)
		velocity.y += (-3000) * delta
		for x in fall.size():
			if fall[x] != null:
				velocity = plataforms[fall[x]].move_and_slide(velocity, Vector3(0,1,0))
	pass

func _on_FloorTimer_timeout():
	#Change mesh to skull and start falling timer
	for x in fall.size():
		if fall[x] != null:
			plataforms[fall[x]].get_node("Model").set_mesh(skullSpace)
			plataforms[fall[x]].get_node("Model").get_mesh().surface_set_material(0, dupe) #Set the duped mesh
	floordownTimer.start()
	pass

func _on_FloordownTimer_timeout():
	#Disable/hide falling plataforms
	restore.clear()
	restore.resize(fall.size())
	for x in fall.size():
		if fall[x] != null:
			plataforms[fall[x]].get_node("CollisionShape").disabled = true
			plataforms[fall[x]].get_node("Model").hide()
			plataforms[fall[x]].set_scale(Vector3(0,0,0))
			restore[x] = fall[x]
		
	restoreTimer.start()
	for x in restore.size():
		if restore[x] != null:
			plataforms[restore[x]].get_node("Model").set_mesh(blueSpace) 
			plataforms[restore[x]].get_node("Model").get_mesh().surface_set_material(0, dupe) #Set the duped mesh
			var t = plataforms[restore[x]].get_translation()
			plataforms[restore[x]].set_translation(Vector3(t.x,0,t.z))
	pass

func _on_RestoreTimer_timeout():
	for x in restore.size():
		if restore[x] != null:
			plataforms[restore[x]].set_scale(Vector3(3,3,3))
	callFallingFloor()
	pass

func callFallingFloor():
	#Set number of falling plataforms
	#floorTimer.set_wait_time(1.75)
	#floordownTimer.set_wait_time(1.75)
	startFloorFall()

#FloorTimer = Timer where plataforms blink
#FloorDownTimer = Timer where plataforms are falling
#RestoreTimers = Plataforms are being restored