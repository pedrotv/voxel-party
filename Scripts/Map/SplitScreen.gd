extends Node2D

onready var mainViewport = $Viewport
onready var mainSprite = $Sprite
onready var mainCamera = $CameraReference

var allPlayers
var mapSetup

var players = []
var cameras = []
var cameraReferences = []
var viewports = []
var sprites = []

var ready = false

export (int) var minDist = 150
export (int) var maxDist = 360

func initiate():
	mapSetup = get_node("../../MapSetup")
	allPlayers = mapSetup.allPlayers
	
	#Active players
	var num = 0
	for x in 4:
		if Global.players[x].type == 1:
			num += 1
	
	#Create new everythings
	players.resize(allPlayers.size())
	cameras.resize(allPlayers.size())
	cameraReferences.resize(allPlayers.size())
	viewports.resize(allPlayers.size())
	sprites.resize(allPlayers.size())
	
	#Get all avaible players
	var a = 0
	for N in allPlayers.size():
		if allPlayers[N] != null:
			players[a] = allPlayers[N]
		
			#Create individual cameras and viewports
			viewports[a] = mainViewport.duplicate()
			add_child(viewports[a])
			
			cameraReferences[a] = mainCamera.duplicate()
			cameras[a] = cameraReferences[a].get_node("CameraAux")
			viewports[a].add_child(cameraReferences[a])
			setViewport(a)
			
			sprites[a] = mainSprite.duplicate()
			$Sprites.add_child(sprites[a])
			setSpritePos(a)
			
			a += 1
	
	#Top camera
	if players.size() == 3:
		viewports.resize(4)
		viewports[3] = mapSetup.get_node("Viewport")
		setViewport(3)
		
		sprites.resize(4)
		sprites[3] = mainSprite.duplicate()
		$Sprites.add_child(sprites[a])
		setSpritePos(3)
		
		sprites[3].show()
		viewports[3].set_clear_mode(2)
		sprites[3].texture = viewports[3].get_texture()
		
	
	#Remove main templates
	mainCamera.get_node("CameraAux/Camera").current = false
	mainViewport.queue_free()
	mainSprite.queue_free()
	
	for N in players.size():
		if players[N] != null:
			cameraReferences[N].translation = Vector3(players[N].translation.x, 50, players[N].translation.z)
	
	#Show lines and camera follow
	if Global.camera == 2:
		splitScreen()
	elif Global.camera == 1:
		singleCameraFollow()
	elif Global.camera == 0:
		singleCamera()
	
	Global.fade.fadeIn()
	ready = true
	pass

func _physics_process(delta):
	if ready:
		if mapSetup.mode == "Podium":
			singleCamera()
		else:
			#Show lines and camera follow
			if Global.camera == 2:
				splitScreen()
			elif Global.camera == 1:
				singleCameraFollow()
			elif Global.camera == 0:
				singleCamera()

func splitScreen():
	get_node("../Camera").current = false
	if players.size() > 1:
		$L/Line2.show()
	if players.size() > 2:
		$L/Line1.show()
	if players.size() == 3:
		sprites[3].show()
		viewports[3].set_clear_mode(2)
		sprites[3].texture = viewports[3].get_texture()
	for N in players.size():
		if players[N] != null:
			sprites[N].show()
			players[N].cam = cameraReferences[N].get_global_transform()
			
			#Camera follow
			var t = players[N].translation
			var c = players[N].cameraAngle
			cameraReferences[N].translation = Vector3(t.x, cameraReferences[N].translation.y, t.z)
			
			#Camera zoom
			cameras[N].translation += Vector3(0, c, c)
			
			#Fix zoom
			if cameras[N].translation.y < 75:
				cameras[N].translation.y = 75
			if cameras[N].translation.y > 210:
				cameras[N].translation.y = 210
			if cameras[N].translation.z < 75:
				cameras[N].translation.z = 75
			if cameras[N].translation.z > 210:
				cameras[N].translation.z = 210
			
			#Camera rot
			cameraReferences[N].rotation_degrees = players[N].cameraRot
			
			#Set viewport sprite
			viewports[N].set_clear_mode(2)
			yield(get_tree(), "idle_frame")
			sprites[N].texture = viewports[N].get_texture()

func singleCamera():
	get_node("../Camera").current = true
	$L/Line2.hide()
	$L/Line1.hide()
	if players.size() == 3:
		sprites[3].hide()
	for N in players.size():
		if players[N] != null:
			sprites[N].hide()
			players[N].cam = get_node("CameraReference").get_global_transform()

func singleCameraFollow():
	var newCam = $CameraReference/CameraAux
	newCam.get_node("Camera").current = true
	$L/Line2.hide()
	$L/Line1.hide()
	if players.size() == 3:
		sprites[3].hide()
	for N in players.size():
		if players[N] != null:
			if N < players.size():
				players[N].cam = get_node("CameraReference").get_global_transform()
				sprites[N].hide()
				if !players[N].alive:
					players.erase(players[N])
					return
	
	#Camera follow 
	var dist = 0
	var t = 0
	if players.size() == 1:
		t = players[0].translation
	if players.size() >= 2:
		dist = players[0].translation.distance_to(players[1].translation)
		t = players[0].translation.linear_interpolate(players[1].translation, 0.5)
	if players.size() == 3:
		dist = t.distance_to(players[2].translation)
		t = players[2].translation.linear_interpolate(t, 0.5)
	if players.size() == 4:
		var aux = players[2].translation.linear_interpolate(players[3].translation, 0.5)
		dist = t.distance_to(aux)
		t = t.linear_interpolate(aux, 0.5)
	
	#Fix dist
	if dist < minDist:
		dist = minDist
	if dist > maxDist:
		dist = maxDist
	
	#Move camera
	if players.size() > 0:
		var pos = Vector3(t.x, mainCamera.translation.y, t.z)
		mainCamera.move_and_slide(pos - mainCamera.translation, Vector3(0,1,0))
	
	#Camera zoom
	if players.size() > 1:
		var aux = Vector3(0, dist, dist) - newCam.translation
		newCam.move_and_slide(aux,Vector3(0,1,0))

func setViewport(a):
	if players.size() > 2:
		viewports[a].set_size(Vector2(Global.width/2,Global.height/2))
	else:
		viewports[a].set_size(Vector2(Global.width/2,Global.height))

func setSpritePos(a):
	if a == 0:
		if players.size() > 2:
			sprites[a].translate(Vector2(Global.width/4,Global.height/4))
		else:
			sprites[a].translate(Vector2(Global.width/4,Global.height/2))
	elif a == 1:
		if players.size() > 2:
			sprites[a].translate(Vector2(Global.width*0.75,Global.height/4))
		else:
			sprites[a].translate(Vector2(Global.width*0.75,Global.height/2))
	elif a == 2:
		sprites[a].translate(Vector2(Global.width/4,Global.height*0.75))
	elif a == 3:
		sprites[a].translate(Vector2(Global.width*0.75,Global.height*0.75))