extends Spatial

var playerScene = preload("res://Scenes/Characters/Player.tscn")
var cpuScene = preload("res://Scenes/Characters/CPU.tscn")
var stageThemes = preload("res://Scenes/Music/AllStageThemes.tscn")

#Timer of the map
onready var matchTimer = $MatchTimer
onready var playerAliveTimer = $PlayerAliveTimer
onready var timerLabel = $Timer/TimerLabel
onready var timerLabel2 = $Timer/TimerLabel2
onready var matchEnd = $MatchEnd
onready var countdown = $Countdown

onready var camera = $Camera
onready var topCamera = $Viewport/CameraTop

#UI containig player name and hearts
var profiles = []
var allPlayers = []
var winner = -1
var matchOn = true

var musicTheme
var r
var allMusics = []

export (String) var mode
export (int) var cameraMode = 1
export (bool) var canAttack = true

func _ready():
	Global.camera = cameraMode
	Global.canAttack = canAttack
	
	#Music
	Global.music.stop()
	var themes = stageThemes.instance()
	add_child(themes)
	for x in themes.get_children():
		allMusics.append(x)
	r = randi()%allMusics.size()
	Global.musicSound(allMusics[r])
	allMusics[r].play()
	
	profiles.resize(4)
	
	#Set timer
	$Timer.show()
	if mode == "Coin": #Coin map
		if int(Global.matchClass.time) > 60:
			matchTimer.set_wait_time(60)
		else:
			matchTimer.set_wait_time(int(Global.matchClass.time))
	else: #Regular map
		matchTimer.set_wait_time(int(Global.matchClass.time))
	timerLabel.text = str(int(matchTimer.wait_time)/60) + ":" + str(int(matchTimer.wait_time)%60)
	if int(matchTimer.wait_time)%60 == 0:
		timerLabel.text = timerLabel.text + "0"
	timerLabel2.text = timerLabel.text
	
	#Set mode name
	countdown.modeName.text = "Get ready!"
	
	for x in 4:
		profiles[x] = get_node("Profiles/PlayerProfile"+str(x))
		
		#Set lives
		Global.players[x].life = int(Global.matchClass.lives)
		if mode == "Grab": 
			profiles[x].uranium = true
		if mode == "Coin":
			profiles[x].coins = true
		
		#Remove character not being used
		if Global.players[x].type == 0:
			get_node("Profiles/PlayerProfile"+str(x)).queue_free()
		elif Global.players[x].type == 1:
			var p = playerScene.instance()
			p.numID = x
			p.set_global_transform(get_node("P/P"+str(x+1)).get_global_transform())
			allPlayers.append(p)
			self.add_child(p)
		else:
			var p = cpuScene.instance()
			p.numID = x
			p.set_global_transform(get_node("P/P"+str(x+1)).get_global_transform())
			allPlayers.append(p)
			self.add_child(p)
	
	get_node("P").queue_free()
	
	#Start splitscreen
	$SplitScreen.initiate()
	pass 

func _physics_process(delta):
	if Input.is_action_just_pressed("ui_force_music_change"):
		allMusics[r].stop()
		r += 1
		if r == allMusics.size():
			r = 0
		allMusics[r].play()
	
	Global.musicSound(allMusics[r])
	
	#Set Timer text
	if !matchTimer.is_stopped():
		timerLabel.text = str(int(matchTimer.get_time_left())/60) + ":" + str(int(matchTimer.get_time_left())%60)
		if int(matchTimer.get_time_left())%60 < 10:
			timerLabel.text = str(int(matchTimer.get_time_left())/60) + ":0" + str(int(matchTimer.get_time_left())%60)
		timerLabel2.text = timerLabel.text
	
	#Start match
	if !countdown.visible && matchTimer.is_stopped() && !matchEnd.visible:
		matchTimer.start()
	
	#Game mode
	gameModeRules()
	pass

#Timer run out (declare tie)
func _on_MatchTimer_timeout():
	gameModeRules(true)
	pass

#Rules for the game mode
func gameModeRules(tie = false):
	if matchOn:
		var alivePlayers = 0
		
		#Check which players are alive
		for x in range (4):
			if Global.players[x].type != 0:
				if Global.players[x].life > 0:
					alivePlayers += 1
		
		#If the game was a tie
		if tie: 
			finishMatch()
			matchEnd.changeName("Draw!")
			Global.matchClass.lastWinner = null
		
		#If player won the race
		elif winner >= 0:
			finishMatch()
			matchEnd.changeName("Player " + str(winner + 1))
			if Global.gameModeType == 0:
				Global.matchClass.lastWinner = winner
				if Global.matchClass.doubleMoney:
					Global.players[winner].coins += 2000
					Global.matchClass.doubleMoney = false
				else:
					Global.players[winner].coins += 1000
			else:
				Global.players[winner].wins += 1
		
		#If only one player was alive
		elif alivePlayers <= 1:
			if playerAliveTimer.is_stopped():
				playerAliveTimer.start()
		
	pass

func finishMatch():
	matchEnd.startTimer()
	matchTimer.set_paused(true)
	matchOn = false
	for x in 4:
		#Give players their coins from the last match
		Global.players[x].coins += Global.players[x].tempCoins
		Global.players[x].tempCoins = 0

func _on_FinishLine_body_shape_entered(body_id, body, body_shape, area_shape):
	if (body.is_in_group("Player")):
		winner = body.numID
	pass

func _on_PlayerAliveTimer_timeout():
	var alivePlayers = 0
	
	#Check which players are alive
	for x in 4:
		if Global.players[x].type != 0:
			if Global.players[x].life > 0:
				alivePlayers += 1
	
	if alivePlayers == 1:
		finishMatch()
		for x in 4:
			if Global.players[x].type != 0 && Global.players[x].life > 0:
				matchEnd.changeName("Player " + str(x + 1))
				if Global.gameModeType == 0:
					Global.matchClass.lastWinner = x
					if Global.matchClass.doubleMoney:
						Global.players[x].coins += 2000
						Global.matchClass.doubleMoney = false
					else:
						Global.players[x].coins += 1000
				else:
					Global.players[x].wins += 1
	else:
		finishMatch()
		matchEnd.changeName("Draw!")
		Global.matchClass.lastWinner = null
	pass
