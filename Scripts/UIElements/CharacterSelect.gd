extends Spatial

onready var modelArrows = $ModelArrows
onready var skinArrows = $SkinArrows
onready var typeModel = $Number

var selected = false
var charModel #The player model
var modelID #The id of the model
var devID #The id of the device 
var skinID = 0 #The id of the player skin
var typeID = 0 #The type of char (none, player, bot/5)

var timer
var currentSkin

var row = 0 #If is changing model or skin
var rot #Model rotation

export (int) var numID

func _ready():
	modelID = numID
	timer = get_node("../Timer")
	timer.set_wait_time(0.25)
	rot = Vector3(0,0,0)
	
	typeID = Global.players[numID].type
	if typeID != 0:
		modelID = Global.players[numID].playerModel
		currentSkin = Global.players[numID].playerSkin
		skinID = Global.players[numID].skinID
		charModel = Global.models[modelID].instance() #instance it
		self.add_child(charModel) #Add to scene
		var aux = charModel.get_node("Armature/Skeleton/Model") #Get model
		aux.set_mesh(aux.get_mesh().duplicate()) #Dupe mesh
		var dupe = aux.get_mesh().surface_get_material(0).duplicate() #Dupe material
		aux.get_mesh().surface_set_material(0, dupe) #Set the duped mesh
		changeSkin(0) #Change skin
		charModel.set_translation(Vector3(0,17.855,0)) #Adjust position
		charModel.set_rotation(rot) #Set rotation
		typeID = 0
		changeType(1)
	
	pass 

func _physics_process(delta):
	
	if charModel != null:
		charModel.get_node("AnimationPlayer").play("Idle",0,1)
	
	#If it's activaded
	if selected:
		
		#Show correct arrows
		modelArrows.hide()
		skinArrows.hide()
		if row == 0:
			modelArrows.show()
		elif row == 1:
			skinArrows.show()
		
		#Instance new model if there's none
		if charModel == null:
			charModel = Global.models[modelID].instance() #instance it
			self.add_child(charModel) #Add to scene
			var aux = charModel.get_node("Armature/Skeleton/Model") #Get model
			aux.set_mesh(aux.get_mesh().duplicate()) #Dupe mesh
			var dupe = aux.get_mesh().surface_get_material(0).duplicate() #Dupe material
			aux.get_mesh().surface_set_material(0, dupe) #Set the duped mesh
			changeSkin(0) #Change skin
			charModel.set_translation(Vector3(0,17.855,0)) #Adjust position
			charModel.set_rotation(rot) #Set rotation
			typeID = 0
			changeType(1)
		
		#Show model
		if typeID == 0:
			charModel.hide()
		else:
			charModel.show()
		
		#Keyboard
		if Global.players[devID].deviceID == 5:
			keyboardControl()
		#joypad
		elif Global.players[devID].deviceID == devID:
			joypadControl()
		
	else: 
		#Hide arrows as it's not selected
		modelArrows.hide()
		skinArrows.hide()
		
		#If isn't player or CPU, hide model
		if !typeModel.visible && charModel != null:
			charModel.hide()
	pass

func keyboardControl():
	if timer.is_stopped():
		#Left and right
		if Input.is_action_just_pressed("ui_move_left"):
			if row == 0:
				changeModel(-1)
			elif row == 1:
				changeSkin(-1)
			elif row == 2:
				changeType(-1)
		if Input.is_action_just_pressed("ui_move_right"):
			if row == 0:
				changeModel(1)
			elif row == 1:
				changeSkin(1)
			elif row == 2:
				changeType(1)
				
		#Up and down
		if Input.is_action_just_pressed("ui_move_up"):
			row -= 1
			if row <= -1:
				row = 2
		if Input.is_action_just_pressed("ui_move_down"):
			row += 1
			if row >= 3:
				row = 0
				
		#Randomize model/skin
		if Input.is_action_just_pressed("ui_random"):
			var r = randi()%Global.numModels
			modelID = 0
			changeModel(r)
			r = randi()%Global.numSkins
			skinID = r
	pass

func joypadControl():
	#Joypad movement
	if timer.is_stopped():
		var direction = Vector2() 
		direction.x = Input.get_joy_axis(devID, 0)
		direction.y = Input.get_joy_axis(devID, 1)
		
		#Deadzone X
		if direction.x >= Global.moveDeadzone || direction.x <= -Global.moveDeadzone:
			timer.start()
			if row == 0:
				changeModel(round(direction.x))
			elif row == 1:
				changeSkin(round(direction.x))
			elif row == 2:
				changeType(round(direction.x))
		#Deadzone Y
		if direction.y >= Global.moveDeadzone || direction.y <= -Global.moveDeadzone:
			timer.start()
			row += round(direction.y)
			if row <= -1:
				row = 2
			if row >= 3:
				row = 0
				
		#D-PAD (left/right)
		if Input.is_action_just_pressed("joy_pad_left_"+str(devID)):
			if row == 0:
				changeModel(-1)
			elif row == 1:
				changeSkin(-1)
			elif row == 2:
				changeType(-1)
		if Input.is_action_just_pressed("joy_pad_right_"+str(devID)):
			if row == 0:
				changeModel(1)
			elif row == 1:
				changeSkin(1)
			elif row == 2:
				changeType(1)
				
		#D-PAD (up/down)
		if Input.is_action_just_pressed("joy_pad_up_"+str(devID)):
			row -= 1
			if row <= -1:
				row = 2
		if Input.is_action_just_pressed("joy_pad_down_"+str(devID)):
			row += 1
			if row >= 3:
				row = 0
		
		#Randomize
		if Input.is_action_just_pressed("joy_pick_"+str(devID)):
			var r = randi()%Global.models.size()
			modelID = 0
			changeModel(r)
			r = randi()%Global.numSkins
			skinID = r
	pass

#Changes type of character: No player = 0, player = 1, CPU = 2
func changeType(num):
	typeID += num
	if typeID <= -1:
		typeID = 2
	if typeID >= 3:
		typeID = 0
	
	#Hide player
	if typeID == 0:
		typeModel.hide()
		charModel.hide()
	#Player
	elif typeID == 1:
		if Global.players[numID].deviceID != null:
			typeModel.show()
			charModel.show()
			typeModel.mesh = Global.typeNumbers[numID]
			
			if numID+1 == 1:
				typeModel.mesh.surface_get_material(0).albedo_color = Color(0.3, 0.3, 0.7, 1)
			elif numID+1 == 2: 
				typeModel.mesh.surface_get_material(0).albedo_color = Color(0.3, 0.7, 0.3, 1)
			elif numID+1 == 3:
				typeModel.mesh.surface_get_material(0).albedo_color = Color(0.7, 0.3, 0.3, 1)
			elif numID+1 == 4:
				typeModel.mesh.surface_get_material(0).albedo_color = Color(0.9, 0.9, 0, 1)
		else:
			changeType(num)
	#CPU
	elif typeID == 2:
		typeModel.show()
		charModel.show()
		typeModel.mesh = Global.typeNumbers[4]
		typeModel.mesh.surface_get_material(0).albedo_color = Color(0.7, 0.7, 0.7, 1)
	
	pass

func changeSkin(num):
	skinID += num
	if skinID <= -1:
		skinID = Global.numSkins - 1
	if skinID >= Global.numSkins:
		skinID = 0
	
	var s = charModel.get_node("Armature/Skeleton/Model").mesh.resource_name #Get Models name
	currentSkin = load("res://Materials/" + s + "/images/"+s+str(skinID + 1)+".png") #Load new skin
	var m = charModel.get_node("Armature/Skeleton/Model").mesh #Get the models mesh
	m.surface_get_material(0).set_texture(0, currentSkin) #Set new skin
	pass

func changeModel(num):
	modelID += num
	if modelID <= -1:
		modelID = Global.models.size() - 1
	if modelID >= Global.models.size():
		modelID = 0
	
	rot = charModel.get_rotation() #Save current player rotation
	charModel.queue_free() #Remove model
	charModel = null #Set reference as null to generate new model
	pass

func sendToMatchClass():
	Global.players[numID].slot = numID #Send player ID
	Global.players[numID].playerModel = modelID #Send ID of the player model
	Global.players[numID].playerSkin = currentSkin #Send skin (Sends the texture, not a integer)
	Global.players[numID].skinID = skinID #Send ID of the skin
	Global.players[numID].type = typeID #Type: No player = 0, player = 1, CPU = 2
	pass
