extends Spatial

onready var model = $Model
onready var timer = $Timer

var start = false
var dir = 0.1

func _physics_process(delta):
	var m = model.get_rotation()
	model.set_rotation(Vector3(m.x + dir * delta, m.y + 1 * delta, m.z + dir * delta))
	pass

func _on_Timer_timeout():
	if dir == 0.1:
		dir = -0.1
	else:
		dir = 0.1
	timer.wait_time = 2
	timer.start()
	pass