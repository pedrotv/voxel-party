extends Control

onready var selectPanel = $SelectPanel
onready var NextPanel = $Next/NextPanel
onready var NextButton = $Next/NextButton
onready var BackPanel = $Back/BackPanel
onready var BackButton = $Back/BackButton
onready var description = $Description/Label
onready var timer = $Timer
onready var sprite1 = $Sprite1
onready var sprite2 = $Sprite2
onready var sprite3 = $Sprite3
onready var sprite4 = $Sprite4
onready var viewport = $Viewport

var row = 6
var options = 7

func _ready():
	timer.set_wait_time(Global.moveTimer)
	Global.fade.fadeIn()
	pass

func _process(delta):
	#Control panel
	if timer.is_stopped():
		if Input.is_action_just_pressed("ui_up"):
			row -= 1
			if row < 0:
				row = options - 1
		if Input.is_action_just_pressed("ui_down"):
			row += 1
			if row > options - 1:
				row = 0
		
		#Increase 
		if row != (options-2) && row != (options-1):
			if Input.is_action_pressed("ui_left"):
				timer.start()
				get_node("AllStats/Stats"+str(row)).changeStat(-1)
			if Input.is_action_pressed("ui_right"):
				timer.start()
				get_node("AllStats/Stats"+str(row)).changeStat(1)
		
		#Go back
		if Input.is_action_just_pressed("ui_cancel"):
			row = options - 2
			ChangeScreen("GamemodeSelectMenu")
	
	#Description
	if row < (options-2):
		var nameSet = get_node("AllStats/Stats"+str(row)).nameSet
		if nameSet == "Time":
			description.text = "How long each map will last. If the time runs out, a draw is declared"
		elif nameSet == "Map":
			description.text = "Choose which maps you will play or play a random one each turn"
		elif nameSet == "Lives":
			description.text = "The number of hearts players have during a match"
		elif nameSet == "Wins":
			description.text = "How many victories a player needs to win the game"
		elif nameSet == "Win money":
			description.text = "The amount of money a player needs to win the game"
		elif nameSet == "Start money":
			description.text = "How much money players have at the start of the game"
		elif nameSet == "Type":
			if Global.gameModeType == 1:
				description.text = "Play a set number of turns or until a player gets enough victories"
			elif Global.gameModeType == 0:
				description.text = "Play a set number of turns or until a player gets enough money to win"
		elif nameSet == "Turns":
			description.text = "How many turns players will play"
	elif row == (options-2):
		description.text = "Go back to the previous page"
	elif row == (options-1):
		description.text = "Go to character select"
	$Description/Label2.text = description.text
	
	#Move arrow
	if has_node("AllStats/Stats"+str(row)):
		selectPanel.rect_position = get_node("AllStats/Stats"+str(row)).rect_position + Vector2(-10,-10)
		
	if row == (options-1):
		selectPanel.hide()
		NextPanel.show()
		BackPanel.hide()
	elif row == (options-2):
		selectPanel.hide()
		BackPanel.show()
		NextPanel.hide()
	else:
		selectPanel.show()
		NextPanel.hide()
		BackPanel.hide()
	
	#Activate selected button
	if Input.is_action_just_pressed("ui_accept"):
		if row == (options-1):
			ChangeScreen("PlayerSelectMenu")
		elif row == (options-2):
			ChangeScreen("GamemodeSelectMenu")
	
	viewport.set_clear_mode(2)
	sprite1.texture = viewport.get_texture()
	sprite2.texture = viewport.get_texture()
	sprite3.texture = viewport.get_texture()
	sprite4.texture = viewport.get_texture()
	
	pass

#Play game (load map/create character)
func ChangeScreen(map):
	#Save match config
	Save.save_game("matchConfigs")
	Global.uiSound.play()
	Global.fade.fadeOut("res://Scenes/Menus/"+str(map)+".tscn")