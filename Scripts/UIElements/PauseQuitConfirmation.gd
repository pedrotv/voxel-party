extends Control

onready var selectPanel = $Menu/SelectPanel

var active = false
var confirm = true
var timer 

func _ready():
	timer = get_node("../Timer")
	pass

func _process(delta):
	if active:
		if Input.is_action_just_pressed("ui_left") || Input.is_action_just_pressed("ui_right"):
			confirm = !confirm
		
		#Move arrow
		if confirm:
			selectPanel.rect_position = get_node("Menu/0").rect_position + Vector2(-10,-10)
		else:
			selectPanel.rect_position = get_node("Menu/1").rect_position + Vector2(-10,-10)
		
		#Activate selected button
		if Input.is_action_just_pressed("ui_accept"):
			if confirm && timer.is_stopped():
				Global.fade.fadeOut("res://Scenes/Menus/PlayerSelectMenu.tscn")
			else:
				PrevScreen()
		
		if Input.is_action_just_pressed("ui_cancel"):
			PrevScreen()
	pass

func PrevScreen():
	if timer.is_stopped():
		timer.start()
		get_node("../../Pause").exiting = false
		get_node("../PauseMenu").show()
		active = false
		self.hide()
