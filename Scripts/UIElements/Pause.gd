extends Control

onready var selectPanel = $PauseMenu/SelectPanel
onready var timer = $Timer
onready var resumeButton = $PauseMenu/Resume/ResumeButton
onready var optionsButton = $PauseMenu/Options/OptionsButton
onready var exitButton = $PauseMenu/Exit/ExitButton
onready var quitConfirmation = $QuitConfirmation

var row = 0
var numButtons = 3
var exiting = false
var quitOn = false

func _ready():
	timer.set_wait_time(Global.moveTimer)
	if Global.gameModeType == 1:
		if Global.matchClass.mapSelection == 0:
			$PauseMenu/Exit/Label.text = "Stages"
			$PauseMenu/Exit/Label2.text = "Stages"
	
	if Global.matchClass.type == 0: #Money/matches to win
		if Global.gameModeType == 0: #Money
			$Stuff1.text = "First player to reach " + str(Global.matchClass.winMoney) + " wins!"
		else: #Matches
			$Stuff1.text = "First player to get " + str(Global.matchClass.wins) + " victories wins!"
	elif Global.matchClass.type == 1: #Turns
		$Stuff1.text = "Current turn: " + str(Global.matchClass.currentTurn) + "/" + str(Global.matchClass.turns)
	$Stuff2.text = $Stuff1.text
	
	pass

func _process(delta):
	if !exiting:
		#Pausa/despausa
		if Input.is_action_just_pressed("ui_pause"):
			if self.is_visible_in_tree():
				self.hide()
				get_tree().paused = false
			else:
				self.show()
				get_tree().paused = true
				row = 0
		
		#Se estiver ativo
		if self.is_visible_in_tree():
			#Controla select panel
			if timer.is_stopped():
				if Input.is_action_just_pressed("ui_up"):
					row -= 1
					if row <= -1:
						row = numButtons-1
				if Input.is_action_just_pressed("ui_down"):
					row += 1
					if row >= numButtons:
						row = 0
				for x in 4:
					if Input.is_action_just_pressed("joy_back_"+str(x)):
						Resume()
			
			#Move panel
			if row == 0:
				selectPanel.set_global_position(resumeButton.get_global_position() + Vector2(-10,-10))
			elif row == 1:
				selectPanel.set_global_position(optionsButton.get_global_position() + Vector2(-10,-10))
			elif row == 2:
				selectPanel.set_global_position(exitButton.get_global_position() + Vector2(-10,-10))
			
			#Activate selected button
			if Input.is_action_just_pressed("ui_accept"):
				#Resume
				if (row == 0):
					Resume()
				elif (row == 1):
					Option()
				elif (row == 2):
					if Global.gameModeType == 1 && Global.matchClass.mapSelection == 0:
						Levels()
					else:
						Exit()
	pass

func Resume():
	self.hide()
	get_tree().paused = false

func Option():
	exiting = true
	$OptionsMenu.show()
	$OptionsMenu.timer.start()
	$PauseMenu.hide()

func Levels():
	exiting = true
	Global.fade.fadeOut("res://Scenes/Menus/MapSelectMenu.tscn")
	pass

func Exit():
	exiting = true
	$QuitConfirmation.show()
	$QuitConfirmation.active = true
	$PauseMenu.hide()
	$Timer.start()