extends Control

onready var selectPanel = $SelectPanel
onready var back = $Back/BackButton
onready var backPanel = $Back/BackPanel
onready var timer = $Timer

var viewport
var active = true
var mapNames = []
var sprite

var row = 0
var currentMap = 0
var numButtons = 7

func _ready():
	if !Global.music.playing:
		Global.music.play()
	
	sprite = get_node("../TVStore/Sprite3D")
	viewport = get_node("../Viewport")
	mapNames.resize(Global.maps.size())
	
	for x in Global.maps.size():
		mapNames[x] = get_node("MapNames/Map"+str(x)+"/Label")
		var aux = Global.maps[x].instance()
		get_node("MapNames/Map"+str(x)+"/Label").set_text(aux.name)
		get_node("MapNames/Map"+str(x)+"/Label2").text = get_node("MapNames/Map"+str(x)+"/Label").text
		aux.queue_free()
	
	ChangeMap()
	
	if Global.matchClass.type == 0: #Money/matches to win
		$Stuff1.text = "First player to get " + str(Global.matchClass.wins) + " victories wins!"
	elif Global.matchClass.type == 1: #Turns
		$Stuff1.text = "Current turn: " + str(Global.matchClass.currentTurn) + "/" + str(Global.matchClass.turns)
	$Stuff2.text = $Stuff1.text
	
	#Start fade
	Global.fade.fadeIn()
	pass

func _process(delta):
	if active:
		#Control panel
		if Input.is_action_just_pressed("ui_up"):
			row -= 1
			if row <= -1:
				row = numButtons - 1
			ChangeMap()
		if Input.is_action_just_pressed("ui_down"):
			row += 1
			if row >= numButtons:
				row = 0
			ChangeMap()
		
		#Move arrow
		if has_node("MapNames/Map"+str(row)):
			selectPanel.rect_position = get_node("MapNames/Map"+str(row)).rect_position + Vector2(-10,-10)
		
		if row == 6:
			backPanel.show()
			selectPanel.hide()
		else:
			backPanel.hide()
			selectPanel.show()
		
		#Activate selected button
		if Input.is_action_just_pressed("ui_accept"):
			if row == (numButtons-1):
				PrevScreen()
			else:
				GoToMap()
		
		if Input.is_action_just_pressed("ui_cancel"):
			PrevScreen()
	
	pass

func ChangeMap():
	if currentMap != row:
		currentMap = row
		
		if row == Global.maps.size()+1:
			sprite.texture = load("res://Sprites/MapPreviews/StageSelect.png")
		elif row == Global.maps.size():
			sprite.texture = load("res://Sprites/MapPreviews/Random.png")
		else:
			sprite.texture = Global.mapPreviews[row]
			
		pass

func GoToMap():
	if row == Global.maps.size():
		Global.fade.fadeOutToRandomMap()
	else:
		Global.currentMap = row
		Global.fade.fadeOutNumber(Global.maps[row])

func PrevScreen():
	if timer.is_stopped():
		timer.start()
		$QuitConfirmation.active = true
		$QuitConfirmation.show()
		active = false
