extends Control

#Buttons
onready var playButton = $Buttons/PlayButton/Play
onready var creditsButton = $Buttons/CreditsButton/Credits
onready var optionsButton = $Buttons/OptionsButton/Options
onready var exitButton = $Buttons/ExitButton/Exit
onready var loadScreen = $Load

onready var space = get_node("../Space")
onready var timer = $Timer

#Arrow
onready var arrow = $Buttons/SelectPanel

#Selected Button
var buttonNum = 0
var animPlayer
var finishedLoad = false

func _ready():
	timer.set_wait_time(Global.moveTimer)
		
	var r = randi()%Global.models.size()
	var a = Global.models[r].instance()
	space.add_child(a)
	a.set_translation(Vector3(0,17.855,0))
	animPlayer = a.get_node("AnimationPlayer")
	
	var rr = randi()%Global.numSkins
	var s = a.get_node("Armature/Skeleton/Model").mesh.resource_name #Get Models name
	var currentSkin = load("res://Materials/" + s + "/images/"+s+str(rr)+".png") #Load new skin
	a.get_node("Armature/Skeleton/Model").mesh.surface_get_material(0).set_texture(0, currentSkin) #Set new skin
	
	Global.fade.fadeIn()
	pass

func _process(delta):
	if !Global.music.playing:
		Global.music.play()
	
	animPlayer.play("Idle")
	
	if $Credits.visible && Input.is_action_just_pressed("ui_cancel"):
		$Credits.hide()
		$Buttons.show()
	
	if $Buttons.visible:
		#Control arrow
		if Input.is_action_just_pressed("ui_up"):
			buttonNum -= 1
			if buttonNum <= -1:
				buttonNum = 3
		if Input.is_action_just_pressed("ui_down"):
			buttonNum += 1
			if buttonNum >= 4:
				buttonNum = 0
				
		#Move arrow
		if buttonNum == 0:
			arrow.rect_position = playButton.rect_position + Vector2(-10,-10)
		elif buttonNum == 1:
			arrow.rect_position = optionsButton.rect_position + Vector2(-10,-10)
		elif buttonNum == 2:
			arrow.rect_position = creditsButton.rect_position + Vector2(-10,-10)
		elif buttonNum == 3:
			arrow.rect_position = exitButton.rect_position + Vector2(-10,-10)
		
		#Activate selected button
		if Input.is_action_just_pressed("ui_accept"):
			if buttonNum == 0:
				PlayGame()
			elif buttonNum == 1:
				$Buttons.hide()
				$OptionsMenu.show()
				$OptionsMenu.timer.start()
			elif buttonNum == 2:
				$Buttons.hide()
				$Credits.show()
			elif buttonNum == 3:
				ExitGame()
	pass

func _input(event):
	if event is InputEventKey and event.pressed:
		if !Input.is_action_pressed("ui_ALT"):
			if !Global.hasKeyboard:
				Global.assignKeyboard()

func PlayGame():
	Global.uiSound.play()
	Global.fade.fadeOut("res://Scenes/Menus/GamemodeSelectMenu.tscn")
	pass

func ExitGame():
	Global.fade.fadeOut("Quit")
	pass

