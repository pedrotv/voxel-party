extends Control

var gameshow = preload("res://Sprites/GamePreviews/GameShow.png")
var regular = preload("res://Sprites/GamePreviews/Regular.png")
var title = preload("res://Sprites/GamePreviews/Title.png")

onready var selectPanel = $SelectPanel

var row = 0

func _ready():
	Global.fade.fadeIn()
	row = Global.gameModeType
	$Map/Sign/AnimationPlayer.play("Wave",0,0.2)
	pass

func _process(delta):
	
	if row == 0:
		$TextLabel.text = "Gameshow mode:\nCompete with players in a gameshow. The player that wins the match gets a random reward from the gameshow panels. Players can buy itens in the shop."
		$Map/Sign/Sprite3D.texture = gameshow
		Global.gameModeType = 0 #Game Show
	elif row == 1:
		$TextLabel.text = "Regular mode:\nPlay matches against players or a CPU. The player with the most victories, wins the game. You can choose the map or each match."
		$Map/Sign/Sprite3D.texture = regular
		Global.gameModeType = 1 #Regular mode
	elif row == 2:
		$TextLabel.text = "Go back to the main menu."
		$Map/Sign/Sprite3D.texture = title
	$TextLabelShadow.text = $TextLabel.text
	
	#Control panel
	if Input.is_action_just_pressed("ui_left"):
		row -= 1
		if row <= -1:
			row = 2
	if Input.is_action_just_pressed("ui_right"):
		row += 1
		if row >= 3:
			row = 0
		
	if Input.is_action_just_pressed("ui_cancel"):
		Global.uiSound.play()
		Global.fade.fadeOut("res://Scenes/Menus/MainMenu.tscn")
	
	#Move arrow
	if has_node("Options/Option"+str(1+row)):
		selectPanel.rect_position = get_node("Options/Option"+str(1+row)).rect_position + Vector2(-10,-10)
	
	#Activate selected button
	if Input.is_action_just_pressed("ui_options"):
		if row == 0:
			Global.fade.fadeOut("res://Scenes/Menus/GameshowRulesSelectMenu.tscn")
			Global.uiSound.play()
		elif row == 1:
			Global.fade.fadeOut("res://Scenes/Menus/RegularRulesSelectMenu.tscn")
			Global.uiSound.play()
	
	#Activate selected button
	if Input.is_action_just_pressed("ui_accept"):
		Global.uiSound.play()
		
		if row == 0:
			Global.matchClass.type = 1
		elif row == 1:
			Global.matchClass.type = 0
		
		if row == 0 || row == 1:
			Global.fade.fadeOut("res://Scenes/Menus/PlayerSelectMenu.tscn")
		elif row == 2:
			Global.fade.fadeOut("res://Scenes/Menus/MainMenu.tscn")
	pass