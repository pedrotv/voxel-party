extends Spatial

var arrows = []

var start = false

func start():
	arrows.resize(4)
	for x in 4:
		if has_node("PlayerArrow"+str(x+1)+"/ArrowHead"):
			arrows[x] = get_node("PlayerArrow"+str(x+1)+"/ArrowHead")
	start = true
	pass

func _physics_process(delta):
	if start:
		for x in 4:
			if arrows[x] != null:
				arrowMovement(arrows[x],delta)
	
	if visible:
		if $Timer.is_stopped():
			$Timer.start()
	else:
		$Timer.stop()
	
	pass

func arrowMovement(arrow,delta):
	#Move arrow
	var rot_speed = rad2deg(0.02)
	var a = arrow.get_rotation()
	arrow.set_rotation(Vector3(a.x,a.y + (-rot_speed) * delta,a.z))

func _on_Timer_timeout():
	Global.showIcons = false
	pass