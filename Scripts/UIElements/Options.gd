extends Control

onready var statName = $StatName
onready var statName2 = $StatName2
onready var statNumber = $StatNumber

onready var leftButton = $LeftButton
onready var rightButton = $RightButton

export(String) var nameSet
var optionSetBool = null
var optionSetInt = null
var off = false 

#Resolution
#Quality

#Voices
#Language

#Buttons
#Sensitivity

#Map/Item config

func _ready():
	if nameSet == "Fullscreen":
		optionSetBool = Global.fullscreen
		if optionSetBool:
			statNumber.text = "ON"
		else:
			statNumber.text = "OFF"
	elif nameSet == "Camera":
		optionSetInt = Global.camera
		if optionSetInt == 0:
			statNumber.text = "Fixed"
		elif optionSetInt == 1:
			statNumber.text = "Follow"
		elif optionSetInt == 2:
			statNumber.text = "Split"
	elif nameSet == "Fps":
		optionSetBool = Global.viewFPS
		if optionSetBool:
			statNumber.text = "ON"
		else:
			statNumber.text = "OFF"
	elif nameSet == "Health Bar":
		optionSetBool = Global.healthBar
		if optionSetBool:
			statNumber.text = "ON"
		else:
			statNumber.text = "OFF"
	elif nameSet == "Shadows":
		optionSetBool = Global.shadow
		if optionSetBool:
			statNumber.text = "ON"
		else:
			statNumber.text = "OFF"
	elif nameSet == "Rumble":
		optionSetBool = Global.rumble
		if optionSetBool:
			statNumber.text = "ON"
		else:
			statNumber.text = "OFF"
	elif nameSet == "Music":
		optionSetInt = Global.musicVolume
		statNumber.text = str(optionSetInt)
	elif nameSet == "SFX":
		optionSetInt = Global.sfxVolume
		statNumber.text = str(optionSetInt)
	else:
		statName.modulate = Color(1,0,0,1)
		statNumber.text = " "
		off = true
	
	statName.text = nameSet
	statName2.text = statName.text
	pass

func changeOption(var dir = 1):
	
	if nameSet == "Fullscreen":
		optionSetBool = !optionSetBool
		Global.fullscreen = optionSetBool
		OS.window_fullscreen = optionSetBool
		if optionSetBool:
			statNumber.text = "ON"
		else:
			statNumber.text = "OFF"
	elif nameSet == "Camera":
		optionSetInt += dir
		if optionSetInt > 2:
			optionSetInt = 0
		elif optionSetInt < 0:
			optionSetInt = 2
		Global.camera = optionSetInt
		if optionSetInt == 0:
			statNumber.text = "Fixed"
		elif optionSetInt == 1:
			statNumber.text = "Follow"
		elif optionSetInt == 2:
			statNumber.text = "Split"
	elif nameSet == "Fps":
		optionSetBool = !optionSetBool
		Global.viewFPS = optionSetBool
		Global.fps_label.visible = optionSetBool
		if optionSetBool:
			statNumber.text = "ON"
		else:
			statNumber.text = "OFF"
	elif nameSet == "Health Bar":
		optionSetBool = !optionSetBool
		Global.healthBar = optionSetBool
		if optionSetBool:
			statNumber.text = "ON"
		else:
			statNumber.text = "OFF"
	elif nameSet == "Shadows":
		optionSetBool = !optionSetBool
		Global.shadow = optionSetBool
		if optionSetBool:
			statNumber.text = "ON"
		else:
			statNumber.text = "OFF"
	elif nameSet == "Rumble":
		optionSetBool = !optionSetBool
		Global.rumble = optionSetBool
		if optionSetBool:
			statNumber.text = "ON"
		else:
			statNumber.text = "OFF"
	elif nameSet == "Music":
		optionSetInt += dir
		if optionSetInt > 10:
			optionSetInt = 10
		elif optionSetInt < 0:
			optionSetInt = 0
		Global.musicVolume = optionSetInt
		statNumber.text = str(optionSetInt)
		Global.musicSound(Global.music)
	elif nameSet == "SFX":
		optionSetInt += dir
		if optionSetInt > 10:
			optionSetInt = 10
		elif optionSetInt < 0:
			optionSetInt = 0
		Global.sfxVolume = optionSetInt
		statNumber.text = str(optionSetInt)
		Global.uiSound.play()
		Global.sfxSound(Global.uiSound)
	
	Global.saveResize()
	pass
