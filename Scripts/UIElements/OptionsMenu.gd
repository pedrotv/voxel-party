extends Control

onready var timer = $Timer

var options = []
var menus = []
var row = 0
var numOptions #Number of options players can change
var lastRow = 0

func _ready():
	timer.set_wait_time(Global.moveTimer)
	
	menus.append($Graphics)
	menus.append($Volume)
	menus.append($Gameplay)
	menus.append($Main)
	
	options.append($Main/Graphics)
	options.append($Main/Volume)
	options.append($Main/Gameplay)
	options.append($Main/Exit)
	pass

func _process(delta):
	#Se estiver ativo
	if self.is_visible_in_tree():
		for x in 4:
			if menus[x].visible:
				numOptions = menus[x].get_child_count() - 1
				
				#Move panel
				menus[x].get_node("SelectPanel").set_position(options[row].get_position() + Vector2(-10,-10))
				
				#Controla select panel
				if Input.is_action_just_pressed("ui_up"):
					row -= 1
					if row <= -1:
						row = numOptions-1
				if Input.is_action_just_pressed("ui_down"):
					row += 1
					if row >= numOptions:
						row = 0
				
				if Input.is_action_just_pressed("ui_left"):
					if x != 3: #Se não for main
						if (row != numOptions-1): # Se não for exit
							options[row].changeOption(-1)
				if Input.is_action_just_pressed("ui_right"):
					if x != 3: #Se não for main
						if (row != numOptions-1): # Se não for exit
							options[row].changeOption(1)
				
				#Go back by pressing B
				if Input.is_action_just_pressed("ui_cancel"):
					if timer.is_stopped():
						timer.start()
						#Exit Options
						if x == 3:
							PrevScreen()
						#Go back to main
						else:
							ChangeTab(x,3)
							row = lastRow
				
				#Activate selected button
				if Input.is_action_just_pressed("ui_accept"):
					if timer.is_stopped():
						timer.start()
						#If is the main option menu
						if x == 3:
							#Resume
							if (row == numOptions-1):
								PrevScreen()
							#Go to another screen option (Graphics, Volume, Gameplay)
							else:
								ChangeTab(x,row)
								lastRow = row
								row = 0
						#If is another (Graphics, Volume, Gameplay)
						else:
							if (row == numOptions-1): #Return to main options menu
								ChangeTab(x,3)
								row = lastRow
	pass

func ChangeTab(x, tab):
	menus[x].hide()
	menus[tab].show()
	#New options
	options.clear()
	for y in menus[tab].get_children():
		if y.name != "SelectPanel":
			options.append(y)

func PrevScreen():
	#Pause Menu (In-game)
	if has_node("../../Pause"):
		get_node("../../Pause").exiting = false
		get_node("../PauseMenu").show()
	#Main Menu
	else:
		get_node("../Buttons").show()
	self.hide()