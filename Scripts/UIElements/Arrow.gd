extends Spatial

var type = 0
var model = null
var modelID = 0
var modelColor = 0
var dir = 0.1
var currentSkin

var deviceControl = -1 #Which joypad is ccontrolling the bot selection

onready var timer = $Timer
onready var number = $Space/Number

export (int) var numID

func _ready():
	$A.hide()
	$SpaceBar.hide()
	$Space.hide()
	pass 

func _physics_process(delta):
	#Player is activated
	if type != 1:
		if Input.is_action_just_pressed("joy_accept_"+str(numID)) || Input.is_action_just_pressed("ui_select") && Global.players[numID].deviceID == 5:
			#If other player was controling, send back
			if type == 2:
				#Keyboard
				if deviceControl == 5:
					for z in 4:
						if get_parent().players[3-z].deviceControl == -1 && get_parent().players[3-z].type == 2 || get_parent().players[3-z].deviceControl == -1 && get_parent().players[3-z].numID == Global.connectedJoypads.size():
							get_parent().players[3-z].deviceControl = 5
				#Controller
				else:
					for y in 4:
						if get_parent().players[y].numID == Global.players[deviceControl].deviceID:
							get_parent().players[y].deviceControl = Global.players[deviceControl].deviceID
							y = 4
			
			type = 1
			deviceControl = Global.players[numID].deviceID
			instancePlayer(numID)
			
	
	#Space bar and A button show and hide
	if type == 0:
		if Global.players[numID].deviceID == 5:
			$SpaceBar.show()
			$A.hide()
		elif Global.players[numID].deviceID != null:
			$A.show()
			$SpaceBar.hide()
		else:
			$A.hide()
			$SpaceBar.hide()
	else:
		$A.hide()
		$SpaceBar.hide()
	
	#Player controling
	if type == 1:
		#Move with arrows/WASD
		if Global.players[numID].deviceID == 5 && deviceControl == 5:
			keyboardMove()
		#Movement with joypad
		elif Global.players[numID].deviceID == numID:
			joypadMove(deviceControl)
		
		playerAnim(delta)
	
	#Player configuring bot
	if type == 2:
		#Move with arrows/WASD
		if deviceControl == 5:
			keyboardMove()
		#Movement with joypad
		else:
			joypadMove(deviceControl)
		
		playerAnim(delta)
		
	pass

#Instance new player model for when player is customizing
func instancePlayer(n):
	$Space.show() #Show arrows/number
	number.mesh = Global.typeNumbers[n] #Set right number or CPU
	
	number.mesh = number.mesh.duplicate()
	var dupe = number.mesh.surface_get_material(0).duplicate() #Dupe material
	number.mesh.surface_set_material(0, dupe) #Set the duped mesh
	
	modelID = randi()%Global.models.size() #Random model
	changeModel(0)
	changeColor(numID)
	timer.wait_time = 1
	timer.start()

#Animation of player rotating
func playerAnim(delta):
	#Idle animation
	model.get_node("AnimationPlayer").play("Idle")
	
	#Player Rotating
	var m = model.get_rotation()
	model.set_rotation(Vector3(m.x + dir * delta, m.y + 1 * delta, m.z + dir * delta))

#Timer for player rotation
func _on_Timer_timeout():
	if dir == 0.1:
		dir = -0.1
	else:
		dir = 0.1
	timer.wait_time = 2
	timer.start()
	pass

func keyboardMove():
	if Input.is_action_just_pressed("ui_move_left"):
		changeModel(-1)
	if Input.is_action_just_pressed("ui_move_right"):
		changeModel(1)
	
	if Input.is_action_just_pressed("ui_move_down"):
		changeColor(-1)
	if Input.is_action_just_pressed("ui_move_up"):
		changeColor(1)
	pass

func joypadMove(id):
	#Change model
	if Input.is_action_just_pressed("joy_pad_left_"+str(id)) || Input.is_action_just_pressed("analog_left_"+str(id)):
		changeModel(-1)
	if Input.is_action_just_pressed("joy_pad_right_"+str(id)) || Input.is_action_just_pressed("analog_right_"+str(id)):
		changeModel(1)
	
	#Change color
	if Input.is_action_just_pressed("joy_pad_down_"+str(id)) || Input.is_action_just_pressed("analog_down_"+str(id)):
		changeColor(-1)
	if Input.is_action_just_pressed("joy_pad_up_"+str(id)) || Input.is_action_just_pressed("analog_up_"+str(id)):
		changeColor(1)
	
	pass

func changeModel(dir):
	modelID += dir
	if modelID < 0:
		modelID = Global.models.size() - 1
	if modelID >= Global.models.size():
		modelID = 0
	
	if model != null:
		model.queue_free()
	model = Global.models[modelID].instance()
	$Space/PlayerModel.add_child(model)
	
	#Make model unique
	model.get_node("Armature/Skeleton/Model").mesh = model.get_node("Armature/Skeleton/Model").mesh.duplicate()
	var dupe = model.get_node("Armature/Skeleton/Model").mesh.surface_get_material(0).duplicate() #Dupe material
	model.get_node("Armature/Skeleton/Model").mesh.surface_set_material(0, dupe) #Set the duped mesh
	
	changeColor(0)
	
	pass

func changeColor(dir):
	modelColor += dir
	if modelColor < 0:
		modelColor = Global.numSkins - 1
	if modelColor >= Global.numSkins:
		modelColor = 0
	
	#Get new skin
	var s = model.get_node("Armature/Skeleton/Model").mesh.resource_name #Get Models name
	currentSkin = load("res://Materials/" + s + "/images/"+s+str(modelColor)+".png") #Load new skin
	model.get_node("Armature/Skeleton/Model").mesh.surface_get_material(0).set_texture(0, currentSkin) #Set new skin
	
	#Number Color
	if modelColor == 0:
		number.mesh.surface_get_material(0).albedo_color = Color(0.25, 0.25, 0.75, 1) #Blue
	elif modelColor == 1:
		number.mesh.surface_get_material(0).albedo_color = Color(0.25, 0.75, 0.25, 1) #Green
	elif modelColor == 2:
		number.mesh.surface_get_material(0).albedo_color = Color(0.75, 0.25, 0.25, 1) #Red
	elif modelColor == 3:
		number.mesh.surface_get_material(0).albedo_color = Color(0.75, 0.75, 0.25, 1) #Yellow
	
	pass

func sendToMatchClass():
	Global.players[numID].slot = numID #Send player ID
	Global.players[numID].playerModel = modelID #Send ID of the player model
	Global.players[numID].playerSkin = currentSkin #Send skin (Sends the texture, not a integer)
	Global.players[numID].type = type #Type: No player = 0, player = 1, CPU = 2
	Global.players[numID].team = modelColor #Team color
	pass
