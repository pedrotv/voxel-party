extends Node

onready var play = $Menu/Buttons/PlayButton/Play
onready var exit = $Menu/Buttons/ExitButton/Exit
onready var playButton = $Menu/Buttons/PlayButton
onready var exitShadow = $Menu/Buttons/ExitButton/ExitShadow

var timer = 0
var players = []

func _ready():
	Global.fade.fadeIn()
	players.append($Player1)
	players.append($Player2)
	players.append($Player3)
	players.append($Player4)
	pass

func _physics_process(delta):
	#Timer for exiting
	if timer < 1:
		timer -= 1 * delta
	else:
		timer = 1
	if timer < 0:
		timer = 0
	exitShadow.margin_right = 150 + (-160 * timer)
	exitShadow.modulate = Color(0.09,0.14,0.31,1)
	
	#Leave button
	if Input.is_action_pressed("ui_cancel"):
		PrevScreen(delta)
	
	#Check if players are ready
	checkPlayers()
	
	#Add/remove bots
	manageBots() 
	
	#Exit
	if Input.is_action_just_pressed("ui_play"):
		if Global.matchClass.numPlayers >= 2:
			NextScreen()
	pass

func manageBots():
	#Add bot (Keyboard)
	if Input.is_action_just_pressed("ui_rotate_right"):
		#Check empty space
		for y in 4:
			if players[y].type == 0: #If there's an empty slot
				players[y].type = 2 #Bot type
				players[y].instancePlayer(4) #Instance the player
				players[y].deviceControl = 5 #Which device controls the bot
				#Remove player control
				for z in 4:
					if players[z] != players[y]:
						if players[z].deviceControl == 5:
							players[z].deviceControl = -1
				return
	#Remove bot (Keyboard)
	if Input.is_action_just_pressed("ui_rotate_left"):
		#Find bot being controlled
		for y in 4:
			if players[y].deviceControl == 5 && players[y].type == 2:
				players[y].type = 0
				players[y].model.hide()
				players[y].get_node("Space").hide()
				players[y].deviceControl = -1
				#Add player control to previous
				for z in 4:
					if players[3-z].deviceControl == -1 && players[3-z].type == 2 || players[3-z].deviceControl == -1 && players[3-z].numID == Global.connectedJoypads.size():
						players[3-z].deviceControl = 5
						return
						
				return
	
	for x in 4:
		#Add bot (Controller)
		if Input.is_action_just_pressed("joy_zoom_in_"+str(x)):
			#Check empty space
			for y in 4:
				if players[y].type == 0: #If there's an empty slot
					players[y].type = 2 #Bot type
					players[y].instancePlayer(4) #Instance the player
					players[y].deviceControl = Global.players[x].deviceID #Which device controls the bot
					#Remove player control
					for z in 4:
						if players[z] != players[y]:
							if players[z].deviceControl == Global.players[x].deviceID:
								players[z].deviceControl = -1
					return
		#Remove bot (Controller)
		if Input.is_action_just_pressed("joy_zoom_out_"+str(x)):
			#Find bot being controlled
			for y in 4:
				if players[y].deviceControl == Global.players[x].deviceID && players[y].type == 2:
					players[y].type = 0
					players[y].model.hide()
					players[y].get_node("Space").hide()
					players[y].deviceControl = -1
					#Add player control to previous
					for z in 4:
						if players[3-z].deviceControl == -1 && players[3-z].type == 2 || players[3-z].deviceControl == -1 && players[3-z].numID == Global.players[x].deviceID:
							players[3-z].deviceControl = Global.players[x].deviceID
							return
							
					return
	pass

func fixColumn(num,b):
	#Fix
	for x in range (4):
		#If spot is already ocuppied
		var a = get_node(str(x+1))
		if b.column == a.column && a.numID != b.numID && a.active:
			b.column += 1
			fixColumn(num,b)
			return

func checkPlayers():
	Global.matchClass.numPlayers = 0
	
	#Send player stats to match class and set number of players
	for x in 4:
		if get_node("Player"+str(x+1)).type != 0:
			Global.matchClass.numPlayers += 1
	
	#Make sure the game doesn't start with less than 2 players
	if Global.matchClass.numPlayers >= 2: 
		playButton.show()
	else: 
		playButton.hide()

func RuleScreen():
	Global.fade.fadeOut("res://Scenes/Menus/RulesSelectMenu.tscn");

func NextScreen():
	if playButton.visible:
		Global.uiSound.play()
		#Gameshow mode
		if Global.gameModeType == 0:
			resetPlayers()
			Global.fade.fadeOut("res://Scenes/GameShow/GameshowStage.tscn")
		#Regular mode
		elif Global.gameModeType == 1:
			resetPlayers()
			if Global.matchClass.mapSelection == 1:
				Global.fade.fadeOutToRandomMap()
			else:
				Global.fade.fadeOut("res://Scenes/Menus/MapSelectMenu.tscn")

func resetPlayers():
	Global.matchClass.lastWinner = null #Reset winner
	Global.matchClass.doubleMoney = false #No double money
	Global.matchClass.currentTurn = 1
	for x in 4:
		get_node("Player"+str(x+1)).sendToMatchClass()
		Global.players[x].tempCoins = 0
		Global.players[x].coins = Global.matchClass.startMoney
		Global.players[x].wins = 0
		Global.players[x].life = int(Global.matchClass.lives)
		Global.players[x].collectables = 0
		Global.players[x].position = 5
		Global.players[x].boughtWeapon = null
		Global.players[x].boughtBoot = null
	pass

func PrevScreen(delta):
	timer += 2 * delta
	exitShadow.modulate = Color(1,0.54,0,1)
	if timer >= 1:
		Global.uiSound.play()
		Global.fade.fadeOut("res://Scenes/Menus/GamemodeSelectMenu.tscn")
