extends Control

export (int) var numID

var viewport

onready var panel = $Panel

func _ready():
	viewport = get_node("../Viewport1")
	
	if Global.players[numID].type == 0:
		self.hide()
	
	#Set color
	if Global.players[numID].team == 0:
		panel.modulate = Color(0.25, 0.25, 0.75, 0.78) #Blue
	elif Global.players[numID].team == 1:
		panel.modulate = Color(0.25, 0.75, 0.25, 0.78) #Green
	elif Global.players[numID].team == 2:
		panel.modulate = Color(0.75, 0.25, 0.25, 0.78) #Red
	elif Global.players[numID].team == 3:
		panel.modulate = Color(0.75, 0.75, 0.25, 0.78) #Yellow
	pass

func _process(delta):
	$Label.text = str(Global.players[numID].wins)
	$LabelShadow.text = $Label.text
	
	viewport.set_clear_mode(2)
	$Sprite.texture = viewport.get_texture()