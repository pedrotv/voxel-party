extends Control

onready var panel = $Panel
onready var panel4 = $Panel4
onready var coins = $Coins
onready var coinsShadow = $CoinsShadow

onready var viewport1 = $Viewport1
onready var playerSprite = $PlayerSprite

var viewport2
onready var coinSprite = $CoinsSprite

export(int) var numID

func _ready():
	viewport2 = get_node("../Viewport2")
	
	if Global.players[numID].type == 2:
		$CPU.show()
	
	#Set color
	if Global.players[numID].team == 0:
		panel.modulate = Color(0.25, 0.25, 0.75, 1) #Blue
	elif Global.players[numID].team == 1:
		panel.modulate = Color(0.25, 0.75, 0.25, 1) #Green
	elif Global.players[numID].team == 2:
		panel.modulate = Color(0.75, 0.25, 0.25, 1) #Red
	elif Global.players[numID].team == 3:
		panel.modulate = Color(0.75, 0.75, 0.25, 1) #Yellow
	
	panel4.modulate = Color(0.71, 0.71, 0.74, 0.8)
	
	var playerModel = $Viewport1/CharacterShots/Model
	$Viewport1/CharacterShots/Model/SceneRoot.queue_free()
	var m = Global.models[Global.players[numID].playerModel].instance()
	playerModel.add_child(m)
	
	var aux = m.get_node("Armature/Skeleton/Model")
	aux.set_mesh(aux.get_mesh().duplicate())
	var dupe = aux.get_mesh().surface_get_material(0).duplicate()
	aux.get_mesh().surface_set_material(0, dupe)
	aux.mesh.surface_get_material(0).set_texture(0,Global.players[numID].playerSkin)
	
	pass

func _process(delta):
	#Get texture
	viewport1.set_clear_mode(2)
	playerSprite.texture = viewport1.get_texture()
	viewport2.set_clear_mode(2)
	coinSprite.texture = viewport2.get_texture()
	
	#Money label
	coins.text = str(Global.players[numID].coins)
	coinsShadow.text = coins.text
	
	pass