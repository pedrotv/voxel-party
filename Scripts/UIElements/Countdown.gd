extends Control

onready var modeName = $Mode/Label
onready var number = $Number/Label
onready var modeName2 = $Mode/Label2
onready var number2 = $Number/Label2
onready var timer = $Timer
onready var bar = $Number/Bar

func _ready():
	pass 

func _process(delta):
	number.text = str(round(timer.get_time_left()))
	if round(timer.get_time_left()) == 0:
		number.text = "Go"
	
	bar.margin_right = 1100 + 95 * timer.time_left
	modeName2.text = modeName.text
	number2.text = number.text
	pass

func _on_countTimer_timeout():
	for x in get_node("../../MapSetup").allPlayers.size():
		get_node("../../MapSetup").allPlayers[x].canMove = true
	self.hide()
	$IconTimer.start()
	pass

func _on_IconTimer_timeout():
	#Global.showIcons = false
	pass