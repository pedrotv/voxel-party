extends Control

onready var panel = $Panel
onready var coins = $Coins
onready var coinsShadow = $CoinsShadow

var viewport2
onready var coinSprite = $CoinsSprite

export(int) var numID

func _ready():
	if Global.players[numID].type == 0:
		self.hide()
	
	viewport2 = get_node("../Viewport2")
	
	#Set color
	if Global.players[numID].team == 0:
		panel.modulate = Color(0.25, 0.25, 0.75, 0.78) #Blue
	elif Global.players[numID].team == 1:
		panel.modulate = Color(0.25, 0.75, 0.25, 0.78) #Green
	elif Global.players[numID].team == 2:
		panel.modulate = Color(0.75, 0.25, 0.25, 0.78) #Red
	elif Global.players[numID].team == 3:
		panel.modulate = Color(0.75, 0.75, 0.25, 0.78) #Yellow
	
	pass

func _process(delta):
	#Get texture
	viewport2.set_clear_mode(2)
	coinSprite.texture = viewport2.get_texture()
	
	#Money label
	coins.text = str(Global.players[numID].coins)
	coinsShadow.text = coins.text
	
	pass