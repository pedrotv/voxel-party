extends Node2D

var scene
var num

var fTime = 4
var fadeInTime = 0.4

onready var anim = $Anim
onready var timer = $Timer

func fadeOut(sceneAdress, pause = true):
	timer.set_wait_time(fadeInTime)
	anim.play("Fade In",0,fTime)
	timer.start()
	get_tree().paused = pause
	scene = sceneAdress

func fadeOutNumber(map, pause = true):
	timer.set_wait_time(fadeInTime)
	anim.play("Fade In",0,fTime)
	timer.start()
	get_tree().paused = true
	scene = "Number"
	num = map

func fadeOutToRandomMap():
	timer.set_wait_time(fadeInTime)
	anim.play("Fade In",0,fTime)
	timer.start()
	get_tree().paused = true
	scene = "Random"

func fadeIn():
	timer.set_wait_time(fadeInTime)
	anim.play("Fade Out",0,fTime)
	timer.start()
	#get_tree().paused = true
	get_tree().paused = false

func _on_Timer_timeout():
	get_tree().paused = false
	
	#Change scene
	if scene != null:
		if scene == "Quit":
			get_tree().quit()
		elif scene == "Random":
			var r = randi()%Global.maps.size()
			get_tree().change_scene_to(Global.maps[r])
			scene = null
		elif scene == "Number":
			get_tree().change_scene_to(num)
			scene = null
		else:
			get_tree().change_scene(scene)
			scene = null
	pass
