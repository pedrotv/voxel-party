extends Control

onready var panel = $Panel
onready var panel4 = $Panel4
onready var hearts = $Hearts
onready var heartsShadow = $HeartsShadow

onready var viewport1 = $Viewport1
onready var playerSprite = $PlayerSprite

var viewport2
onready var heartSprite = $HeartSprite

var viewport3
var viewport4
onready var collectableSprite = $Collectable/CollectableSprite
onready var collectable = $Collectable
onready var collectableLabel = $Collectable/Label
onready var collectableLabelShadow = $Collectable/LabelShadow

var uranium = false
var coins = false

export(int) var numID

func _ready():
	viewport2 = get_node("../Viewport2")
	viewport3 = get_node("../Viewport3")
	viewport4 = get_node("../Viewport4")
	
	if Global.players[numID].type == 2:
		$CPU.show()
	
	#Set color
	if Global.players[numID].team == 0:
		panel.modulate = Color(0.25, 0.25, 0.75, 1) #Blue
	elif Global.players[numID].team == 1:
		panel.modulate = Color(0.25, 0.75, 0.25, 1) #Green
	elif Global.players[numID].team == 2:
		panel.modulate = Color(0.75, 0.25, 0.25, 1) #Red
	elif Global.players[numID].team == 3:
		panel.modulate = Color(0.75, 0.75, 0.25, 1) #Yellow
	
	panel4.modulate = Color(0.71, 0.71, 0.74, 0.8)
	
	var playerModel = $Viewport1/CharacterShots/Model
	$Viewport1/CharacterShots/Model/SceneRoot.queue_free()
	var m = Global.models[Global.players[numID].playerModel].instance()
	playerModel.add_child(m)
	
	var aux = m.get_node("Armature/Skeleton/Model")
	aux.set_mesh(aux.get_mesh().duplicate())
	var dupe = aux.get_mesh().surface_get_material(0).duplicate()
	aux.get_mesh().surface_set_material(0, dupe)
	aux.mesh.surface_get_material(0).set_texture(0,Global.players[numID].playerSkin)
	
	pass
	
func _process(delta):
	if coins || uranium:
		collectable.show()
	
	#Get texture
	viewport1.set_clear_mode(2)
	playerSprite.texture = viewport1.get_texture()
	viewport2.set_clear_mode(2)
	heartSprite.texture = viewport2.get_texture()
	
	#Uranium label
	if uranium:
		viewport3.set_clear_mode(2)
		collectableSprite.texture = viewport3.get_texture()
		collectableLabel.text = str(Global.players[numID].collectables) + " / " + str(Global.uraniumValue)
		collectableLabelShadow.text = collectableLabel.text
	
	#Coin label
	if coins:
		viewport4.set_clear_mode(2)
		collectableSprite.texture = viewport4.get_texture()
		collectableLabel.text = "$" + str(Global.players[numID].tempCoins)
		collectableLabelShadow.text = collectableLabel.text
	
	hearts.text = str(Global.players[numID].life)
	heartsShadow.text = hearts.text
	
	pass