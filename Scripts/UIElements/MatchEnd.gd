extends Control

onready var timer = $matchTimer
onready var playerName = $PlayerName/Label
onready var playerName2 = $PlayerName/Label2

var goToPodium = false

func _ready():
	for x in 4:
		if Global.gameModeType == 0:
			$CoinProfiles.show()
			if Global.players[x].type == 0:
				get_node("CoinProfiles/CoinsProfile"+str(x+1)).hide()
		else:
			$TrophyProfiles.show()
			if Global.players[x].type == 0:
				get_node("TrophyProfiles/TrophyProfile"+str(x+1)).hide()

func startTimer():
	self.show()
	if timer.is_stopped():
		timer.start()
	pass

func _on_matchTimer_timeout():
	if Global.gameModeType == 0: #Gameshow
		#Check if go to podium
		if Global.matchClass.type == 0:
			for x in 4:
				if Global.players[x].coins >= Global.matchClass.winMoney:
					goToPodium = true
		else:
			Global.matchClass.currentTurn += 1
			if Global.matchClass.currentTurn > Global.matchClass.turns:
				goToPodium = true
		
		#Go to gameshow stage
		if !goToPodium:
			Global.fade.fadeOut("res://Scenes/GameShow/GameshowStage.tscn")
		#Go to podium
		else:
			Global.fade.fadeOut("res://Scenes/Maps/Podium.tscn")
			
	elif Global.gameModeType == 1: #Regular
		#Check if go to podium
		if Global.matchClass.type == 0:
			for x in 4:
				if Global.players[x].wins == int(Global.matchClass.wins):
					goToPodium = true
		else:
			Global.matchClass.currentTurn += 1
			if Global.matchClass.currentTurn > Global.matchClass.turns:
				goToPodium = true
		
		if !goToPodium:
			#Go to next match
			if Global.matchClass.mapSelection == 1:
				Global.fade.fadeOutToRandomMap()
			else:
				Global.fade.fadeOut("res://Scenes/Menus/MapSelectMenu.tscn")
		#Go to podium
		else:
			Global.fade.fadeOut("res://Scenes/Maps/Podium.tscn")
	pass

func changeName(name):
	if name == "Draw!":
		playerName.text = str(name)
	else:
		playerName.text = str(name) + " won!"
	playerName2.text = playerName.text