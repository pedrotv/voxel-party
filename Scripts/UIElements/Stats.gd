extends Control

onready var statName = $StatName
onready var statNumber = $StatNumber

onready var leftButton = $LeftButton
onready var rightButton = $RightButton

var stat 

export(String) var nameSet

func _ready():
	if nameSet == "Win money" || nameSet == "Wins": 
		if Global.matchClass.type == 1:
			nameSet = "Turns"
	
	initiate()

func initiate():
	statName.text = nameSet
	
	if nameSet == "Time":
		stat = Global.matchClass.time
		statNumber.text = str(stat/60) + ":" + str(stat%60)
		if stat%60 == 0:
			statNumber.text = statNumber.text + "0"
	elif nameSet == "Map":
		stat = Global.matchClass.mapSelection
		if stat == 0:
			statNumber.text = "Choose"
		elif stat == 1:
			statNumber.text = "Random"
	elif nameSet == "Lives":
		stat = Global.matchClass.lives
		statNumber.text = str(stat)
	elif nameSet == "Wins":
		stat = Global.matchClass.wins
		statNumber.text = str(stat)
	elif nameSet == "Win money":
		stat = Global.matchClass.winMoney
		statNumber.text = str(stat)
	elif nameSet == "Start money":
		stat = Global.matchClass.startMoney
		statNumber.text = str(stat)
	elif nameSet == "Type":
		stat = Global.matchClass.type
		if stat == 0:
			statNumber.text = "Wins"
		elif stat == 1:
			statNumber.text = "Turns"
	elif nameSet == "Turns":
		stat = Global.matchClass.turns
		statNumber.text = str(stat)
	
	$StatName2.text = statName.text
	
	pass

func changeStat(dir):
	if nameSet == "Wins":
		stat += dir
		if stat < 1:
			stat = 99
		elif stat > 99:
			stat = 1
		statNumber.text = str(stat)
		Global.matchClass.wins = stat
	elif nameSet == "Lives":
		stat += dir
		if stat < 1:
			stat = 99
		elif stat > 99:
			stat = 1
		statNumber.text = str(stat)
		Global.matchClass.lives = stat
	elif nameSet == "Turns":
		stat += dir
		if stat < 1:
			stat = 99
		elif stat > 99:
			stat = 1
		statNumber.text = str(stat)
		Global.matchClass.turns = stat
	elif nameSet == "Time":
		stat += dir * 15
		if stat < 15:
			stat = 600
		elif stat > 600:
			stat = 15
		statNumber.text = str(stat/60) + ":" + str(stat%60)
		if stat%60 == 0:
			statNumber.text = statNumber.text + "0"
		Global.matchClass.time = stat
	elif nameSet == "Map":
		stat += dir
		if stat < 0:
			stat = 1
		elif stat > 1:
			stat = 0
		if stat == 0:
			statNumber.text = "Choose"
		elif stat == 1:
			statNumber.text = "Random"
		Global.matchClass.mapSelection = stat
	elif nameSet == "Win money":
		stat += dir * 1000
		if stat < 0:
			stat = 50000
		elif stat > 50000:
			stat = 0
		statNumber.text = str(stat)
		Global.matchClass.winMoney = stat
	elif nameSet == "Start money":
		stat += dir * 100
		if stat < 0:
			stat = 2500
		elif stat > 2500:
			stat = 0
		statNumber.text = str(stat)
		Global.matchClass.startMoney = stat
	elif nameSet == "Type":
		stat += dir
		if stat < 0:
			stat = 1
		elif stat > 1:
			stat = 0
		if stat == 0:
			statNumber.text = "Wins"
			if Global.gameModeType == 0: #Win Money
				get_node("../Stats1").nameSet = "Win money"
			else:
				get_node("../Stats1").nameSet = "Wins"
		elif stat == 1:
			statNumber.text = "Turns"
			get_node("../Stats1").nameSet = "Turns"
		get_node("../Stats1").initiate()
		Global.matchClass.type = stat
	pass