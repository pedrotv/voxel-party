extends Spatial

var dir = Vector3(0,0,0)
var force = 10

func _ready():
	print(dir)
	for x in 12:
		get_node("Particle"+str(x+1)).apply_impulse(Vector3(1,1,1), Vector3(dir.x + force, 0, dir.z + force))
	pass

func _process(delta):
	pass

func _on_Timer_timeout():
	self.queue_free()
	pass # Replace with function body.
