extends Node

var slot #The slot the player is occupying (1, 2, 3 or 4)
var type = 0 #Type of player (0 == None, 1 == Player controlled, 2 == CPU)
var playerModel #The model choosen by the player
var deviceID #The ID of the device the player is using to play (joypad or keyboard)
var playerSkin #The skin of the model
var team #Color of team

var coins = 0 #Number of coins the players has
var life = 20 #The players current life
var wins = 0 #Number of wins the player currently has
var collectables = 0
var tempCoins = 0
var position = 5 #Position on the podium at the end

var boughtWeapon = null
var boughtBoot = null