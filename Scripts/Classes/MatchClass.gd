extends Node

onready var player1 = $PlayerClass1
onready var player2 = $PlayerClass2
onready var player3 = $PlayerClass3
onready var player4 = $PlayerClass4

var numPlayers = 0

var turns = 3
var startMoney = 500
var winMoney = 10000
var wins = 5
var lives = 25
var time = 150
var mapSelection = 0
var type = 1

var currentTurn = 1
var lastWinner = null
var doubleMoney = false