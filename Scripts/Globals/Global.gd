extends Node

#Assests that need loading

var models = []
var deadModels = []
var weapons = []
var maps = []
var coinMaps = []
var mapPreviews = []
var coinMapPreviews = []
var typeNumbers = []
var gameShowPanels = []
var purpleSeaPlataforms = []

var bootL = preload("res://Scenes/Weapons/JetBootsL.tscn")

var numSkins = 4 #Number of skin the game currently has

var fpsLabelScene = preload("res://Scenes/UIElements/FPSLabel.tscn")
var matchClassScene = preload("res://Scenes/Class/MatchClass.tscn")
var fadeScene = preload("res://Scenes/UIElements/Fade.tscn")
var musicScene = preload("res://Scenes/Music/Music.tscn")
var UISoundScene = preload("res://Scenes/Music/UISound.tscn")

#------------------------------

#Config options
var viewFPS = false
var itens = true
var rumble = true
var healthBar = true
var fullscreen = true
var camera = 1

var width = 1920
var height = 1080

#------------------------------

var music
var musicVolume = 10

var uiSound
var sfxVolume = 10

var shadow = true
var currentMap 
var fps_label #FPS instance
var matchClass #Class that holds all the match and players configurations
var fade #Fade instance
var players = [] #Array storing all player classes

var connectedJoypads #Array of connected joypads
var hasKeyboard = false
var keyboardID = 5 #Which player is using the keyboard

var moveTimer = 0.2
var moveDeadzone = 0.2

var specialMap = 0 ### 0 = Regular map / 1 = Coin map / 2 = Bad Coin Map
var uraniumValue = 15
var showIcons = true
var gameModeType = 0  ### Gameshow Mode = 0 / Regular Mode = 1
var canAttack = true
var devShortcuts = false

func loadPlataforms():
	#Load plataforms
	for x in 7:
		purpleSeaPlataforms.append(load("res://Models/ObjType/PurpleSeaPlataforms/"+str(x)+".obj"))

func loadModels():
	#Load Models
	models.append(preload("res://Scenes/RiggedChars/Zombie.tscn"))
	models.append(preload("res://Scenes/RiggedChars/Knight.tscn"))
	models.append(preload("res://Scenes/RiggedChars/Hero.tscn"))
	models.append(preload("res://Scenes/RiggedChars/RedSkull.tscn"))
	models.append(preload("res://Scenes/RiggedChars/Mage.tscn"))
	pass

func loadDeadModels():
	#Load Models
	deadModels.append(preload("res://Scenes/Characters/DeadZombie.tscn"))
	deadModels.append(preload("res://Scenes/Characters/DeadKnight.tscn"))
	deadModels.append(preload("res://Scenes/Characters/DeadHero.tscn"))
	deadModels.append(preload("res://Scenes/Characters/DeadRedSkull.tscn"))
	deadModels.append(preload("res://Scenes/Characters/DeadMage.tscn"))
	pass

func loadMaps():
	#Load Maps
	maps.append(preload("res://Scenes/Maps/IslandRace.tscn"))
	maps.append(preload("res://Scenes/Maps/LavaLand.tscn"))
	maps.append(preload("res://Scenes/Maps/Fall3X3.tscn"))
	maps.append(preload("res://Scenes/Maps/PurpleSea.tscn"))
	maps.append(preload("res://Scenes/Maps/Factory.tscn"))
	
	#Map previews
	mapPreviews.append(preload("res://Sprites/MapPreviews/IslandRace.png"))
	mapPreviews.append(preload("res://Sprites/MapPreviews/LavaLand.png"))
	mapPreviews.append(preload("res://Sprites/MapPreviews/Fall3X3.png"))
	mapPreviews.append(preload("res://Sprites/MapPreviews/PurpleSea.png"))
	mapPreviews.append(preload("res://Sprites/MapPreviews/Factory.png"))
	
	#Coin maps
	coinMaps.append(preload("res://Scenes/Maps/Circus.tscn"))
	
	#Coin map previews
	coinMapPreviews.append(preload("res://Sprites/MapPreviews/Circus.png"))

func loadTypeNUmbers():
	#Load type numbers
	typeNumbers.append(preload("res://Models/ObjType/Numbers/1.obj"))
	typeNumbers.append(preload("res://Models/ObjType/Numbers/2.obj"))
	typeNumbers.append(preload("res://Models/ObjType/Numbers/3.obj"))
	typeNumbers.append(preload("res://Models/ObjType/Numbers/4.obj"))
	typeNumbers.append(preload("res://Models/ObjType/Numbers/5.obj"))

func loadWeapons():
	#Load weapons
	weapons.append(preload("res://Scenes/Weapons/Bomb.tscn"))
	weapons.append(preload("res://Scenes/Weapons/Buster.tscn"))
	weapons.append(preload("res://Scenes/Weapons/BoxingGlove.tscn"))
	weapons.append(preload("res://Scenes/Weapons/Hammer.tscn"))
	weapons.append(preload("res://Scenes/Weapons/JetBootsR.tscn"))

func loadPanels():
	#Load Game Show Panels
	gameShowPanels.append(preload("res://Models/ObjType/GameshowPanels/2XPanel.obj"))
	gameShowPanels.append(preload("res://Models/ObjType/GameshowPanels/BankruptPanel.obj"))
	gameShowPanels.append(preload("res://Models/ObjType/GameshowPanels/CoinPanel1.obj"))
	gameShowPanels.append(preload("res://Models/ObjType/GameshowPanels/CoinPanel2.obj"))
	gameShowPanels.append(preload("res://Models/ObjType/GameshowPanels/CoinPanel3.obj"))
	gameShowPanels.append(preload("res://Models/ObjType/GameshowPanels/RedistribuitionPanel.obj"))
	gameShowPanels.append(preload("res://Models/ObjType/GameshowPanels/CoinMinigamePanel.obj"))

func _ready():
	#Random numbers
	randomize()
	
	#Load Assets
	loadPlataforms()
	loadModels()
	loadDeadModels()
	loadMaps()
	loadTypeNUmbers()
	loadWeapons()
	loadPanels()
	
	#Instance fps label
	fps_label = fpsLabelScene.instance()
	add_child(fps_label)
	
	#Instance match class
	matchClass = matchClassScene.instance()
	add_child(matchClass)
	
	#Instace Fade
	fade = fadeScene.instance()
	add_child(fade)
	
	#Instance SFX
	uiSound = UISoundScene.instance()
	add_child(uiSound)
	
	#Instance music
	music = musicScene.instance()
	add_child(music)
	
	#Instance player classes (not the player characters in game)
	players.resize(4)
	players[0] = matchClass.player1
	players[1] = matchClass.player2
	players[2] = matchClass.player3
	players[3] = matchClass.player4
	
	#Load save
	#Save.save_game("matchConfigs")
	Save.load_game("matchConfigs")
	Save.load_game("saveConfigs")
	OS.window_fullscreen = fullscreen
	fps_label.visible = viewFPS
	get_tree().get_root().connect("size_changed", self, "saveResize")
	
	#Every time a new controller is connected
	Input.connect("joy_connection_changed", self, "_on_joy_connection_changed")
	
	#Checks for connected joypads and assigns them to players
	connectedJoypads = Input.get_connected_joypads()
	for x in range (connectedJoypads.size()):
		players[x].deviceID = connectedJoypads[x]
	assignKeyboard()
	#players[0].deviceID = 0
	#players[1].deviceID = 1
	
	pass

func _process(delta):
	#Make mouse invisible in fullscreen
	if fullscreen:
		Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	else:
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	
#	#Fullscreen
#	if Input.is_action_pressed("ui_ALT"):
#		if Input.is_action_just_pressed("ui_fullscreen"):
#			OS.window_fullscreen = !OS.window_fullscreen
#			fullscreen = OS.window_fullscreen
#			saveResize()
	
	#FPS label
	fps_label.get_node("Label").text = str(Engine.get_frames_per_second())
	
	musicSound(music)
	sfxSound(uiSound)
	
	pass

func musicSound(m):
	#Set music volume
	m.volume_db = -40 + musicVolume * 2
	if musicVolume == 0:
		m.volume_db = -80

func sfxSound(s):
	#Set sfx volume
	s.volume_db =  -35 + sfxVolume * 5
	if sfxVolume == 0:
		s.volume_db = -80

func sfx3DSound(s):
	#Set sfx volume
	s.unit_db =  -35 + sfxVolume * 5
	if sfxVolume == 0:
		s.unit_db = -80

func saveResize():
	Save.save_game("saveConfigs")

func _on_joy_connection_changed(device_id, connected):
	#Reset devide ID (change this later)
	for x in 4:
		players[x].deviceID = null
		
	#Checks for connected joypads and assigns them to players
	connectedJoypads = Input.get_connected_joypads()
	for x in range (connectedJoypads.size()):
		players[x].deviceID = connectedJoypads[x]
		
	if hasKeyboard:
		assignKeyboard()
	pass

func assignKeyboard():
	hasKeyboard = true
	players[connectedJoypads.size()].deviceID = keyboardID
	pass

func removeKeyboard():
	hasKeyboard = false
	players[connectedJoypads.size()].deviceID = null
	pass