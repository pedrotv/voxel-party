extends Node

func saveConfigs():
	var save_dict = {
		"viewFPS" : Global.viewFPS,
		"fullscreen" : Global.fullscreen,
		"rumble" : Global.rumble,
		"healthBar" : Global.healthBar,
		"shadow" : Global.shadow,
		"camera" : Global.camera,
		"musicVolume" : Global.musicVolume,
		"sfxVolume" : Global.sfxVolume,
		"windowSizeX" : OS.window_size.x,
		"windowSizeY" : OS.window_size.y,
		"windowPositionX" : OS.window_position.x,
		"windowPositionY" : OS.window_position.y
	}
	return save_dict

func matchConfigs():
	var save_dict = {
		"type" : Global.matchClass.type,
		"turns" : Global.matchClass.turns,
		"winMoney" : Global.matchClass.winMoney,
		"startMoney" : Global.matchClass.startMoney,
		"wins" : Global.matchClass.wins,
		"lives" : Global.matchClass.lives,
		"time" : Global.matchClass.time,
		"mapSelection" : Global.matchClass.mapSelection
	}
	return save_dict

func save_game(name):
	var save_game = File.new()
	save_game.open("user://"+str(name)+".save", File.WRITE)
	
	var node_data = call(name);
	save_game.store_line(to_json(node_data))
	
	save_game.close()

func load_game(name):
	var save_game = File.new()
	if not save_game.file_exists("user://"+str(name)+".save"):
		print("Couldn't find file, creating one")
		save_game(name)
		return
	
	save_game.open("user://"+str(name)+".save", File.READ)
	var current_line = parse_json(save_game.get_line())
	if name == "saveConfigs":
		Global.viewFPS = current_line["viewFPS"]
		Global.fullscreen = current_line["fullscreen"]
		Global.rumble = current_line["rumble"]
		Global.shadow = current_line["shadow"]
		Global.healthBar = current_line["healthBar"]
		Global.camera = int(current_line["camera"])
		Global.musicVolume = int(current_line["musicVolume"])
		Global.sfxVolume = int(current_line["sfxVolume"])
		OS.window_size = Vector2(int(current_line["windowSizeX"]),int(current_line["windowSizeY"]))
		OS.window_position = Vector2(int(current_line["windowPositionX"]),int(current_line["windowPositionY"]))
	elif name == "matchConfigs":
		Global.matchClass.type = int(current_line["type"])
		Global.matchClass.turns = int(current_line["turns"])
		Global.matchClass.winMoney = int(current_line["winMoney"])
		Global.matchClass.startMoney = int(current_line["startMoney"])
		Global.matchClass.wins = int(current_line["wins"])
		Global.matchClass.lives = int(current_line["lives"])
		Global.matchClass.time = int(current_line["time"])
		Global.matchClass.mapSelection = int(current_line["mapSelection"])
	
	save_game.close()
