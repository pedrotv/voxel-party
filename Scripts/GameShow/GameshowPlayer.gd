extends Spatial

var initPosition = Vector3()

var animPlayer
var model
var skeleton

func ready(numID):
	$Hero.queue_free()
	
	#Change mesh
	var playerModel = $PlayerModel
	var m = Global.models[Global.players[numID].playerModel].instance()
	playerModel.add_child(m)
	
	var aux = m.get_node("Armature/Skeleton/Model")
	aux.set_mesh(aux.get_mesh().duplicate())
	var dupe = aux.get_mesh().surface_get_material(0).duplicate()
	aux.get_mesh().surface_set_material(0, dupe)
	aux.mesh.surface_get_material(0).set_texture(0,Global.players[numID].playerSkin)
	
	if Global.players[numID].type == 0:
		self.queue_free()
	
	animPlayer = m.get_node("AnimationPlayer")
	
	pass

func _physics_process(delta):
	if animPlayer != null:
		animPlayer.play("Idle")
	pass