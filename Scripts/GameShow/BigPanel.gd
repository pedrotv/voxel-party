extends Spatial

var panels = []
var nextPanels = []
var speed = []
var chosenPanel

var velocity = Vector3(0,0,0)
var nextSpeed = 500
var dir = -1
var turns = 0 #0 = columns spinning, 1 = middle line spining, 2 = all stopped

onready var stopArea1 = $Portal/StopArea1
onready var label = $Portal/Result/Label
onready var labelShadow = $Portal/Result/LabelShadow
onready var textureRect = $Portal/Result/TextureRect

func _ready():
	$Portal/Result/A.hide()
	$Portal/Result/Space.hide()
	#Set matrix
	var a = 0
	for x in self.get_children():
		if x.name != "Portal":
			panels.append([])
			for y in x.get_children():
				panels[a].append(y)
				#Randomize panels
				y.get_node("MeshInstance").mesh = Global.gameShowPanels[randi()%Global.gameShowPanels.size()]
			a += 1
	
	get_node("Portal/ExtraPanel/MeshInstance").mesh = Global.gameShowPanels[randi()%Global.gameShowPanels.size()]
	speed.resize(9)
	for x in 9:
		speed[x] = randi()%100 + 50
	pass

func _physics_process(delta):
	velocity = Vector3(0,0,0)
	#Move up/down
	if Global.matchClass.lastWinner != null:
		if turns == 0 || turns == 1:
			dir = -1
			for column in self.get_child_count() -1:
				velocity.y = speed[column] * dir
				for panel in get_node("Column"+str(column+1)).get_children():
					panel.move_and_slide(velocity, Vector3(0,1,0))
				
				if dir == 1:
					dir = -1
				else:
					dir = 1
	
	#Turn panels backwards
	if turns == 2:
		var rot_speed = rad2deg(0.05)
		for x in panels.size(): #Turn all panels backwards
			for y in panels[x].size():
				var a = panels[x][y].get_rotation()
				panels[x][y].set_rotation(Vector3(a.x, a.y, a.z + (rot_speed) * delta))
				if a.z >= deg2rad(180):
					turns = 3
		for n in nextPanels.size(): #Fix next panels
			nextPanels[n].set_rotation(Vector3(0,0,0))
	
	#Move sideways
	if turns == 3 || turns == 4:
		for x in nextPanels.size():
			velocity.x = nextSpeed
			nextPanels[x].set_rotation(Vector3(0,0,0))
			nextPanels[x].move_and_slide(velocity, Vector3(0,1,0))
	
	#Turn nextpanels backwards
	if turns == 5:
		var rot_speed = rad2deg(0.05)
		for n in nextPanels.size(): #Fix next panels
			var a = nextPanels[n].get_rotation()
			nextPanels[n].set_rotation(Vector3(a.x, a.y, a.z + (rot_speed) * delta))
			if a.z >= deg2rad(180):
				turns = 6
		chosenPanel.set_rotation(Vector3(0,0,0))
	
	if turns == 6:
		showResults()
		turns = 7
	
	pass

func _on_Area1_body_shape_entered(body_id, body, body_shape, area_shape):
	if (body.is_in_group("UpPanel")):
		body.translation = body.translation + Vector3(0,0,227.5)
	pass

func _on_Area2_body_shape_entered(body_id, body, body_shape, area_shape):
	if (body.is_in_group("DownPanel")):
		body.translation = body.translation + Vector3(0,0,-227.5)
	pass

func _on_StopArea1_body_shape_entered(body_id, body, body_shape, area_shape):
	if turns == 1:
		#Grab item
		for x in panels.size():
			for y in panels[x].size():
				if panels[x][y] == body:
					speed[x] = 0
					var alreadyAdded = false
					for n in nextPanels.size():
						if nextPanels[n] == body:
							alreadyAdded = true
					if !alreadyAdded:
						nextPanels.append(body)
					body.translation.z = 0
					
					var aux = 0
					for sx in speed.size():
						if speed[sx] == 0:
							aux += 1
					if aux == 9:
						turns += 1
						nextPanels.append($Portal/ExtraPanel)
	pass

func _on_TPArea3_body_shape_entered(body_id, body, body_shape, area_shape):
	body.translation = body.translation + Vector3(-325,0,0)
	pass

func _on_StopArea2_body_shape_entered(body_id, body, body_shape, area_shape):
	if turns == 4:
		#Grab item
		for x in nextPanels.size():
			if nextPanels[x] == body:
				nextSpeed = 0
				chosenPanel = body
				turns = 5
	pass

func showResults():
	for y in Global.gameShowPanels.size():
		if chosenPanel.get_node("MeshInstance").mesh == Global.gameShowPanels[y]:
			$Portal/Result.show()
			$Portal/Result/A.hide()
			$Portal/Result/Space.hide()
			if y == 0:
				label.text = "Double Money next round!"
				Global.matchClass.doubleMoney = true
			elif y == 1:
				label.text = "Give away half of your money!"
				Global.players[Global.matchClass.lastWinner].coins = round(Global.players[Global.matchClass.lastWinner].coins/2)
			elif y == 2:
				label.text = "Gain 200 coins!"
				Global.players[Global.matchClass.lastWinner].coins += 200
			elif y == 3:
				label.text = "Gain 500 coins!"
				Global.players[Global.matchClass.lastWinner].coins += 500
			elif y == 4:
				label.text = "Gain 800 coins!"
				Global.players[Global.matchClass.lastWinner].coins += 800
			elif y == 5:
				label.text = "All the money gets redistributed!"
				var newMoney = 0
				var numPlayers = 0
				for x in 4:
					if Global.players[x].type != 0:
						newMoney += Global.players[x].coins
						numPlayers += 1
				for x in 4:
					if Global.players[x].type != 0:
						Global.players[x].coins = round(newMoney/numPlayers)
			elif y == 6:
				label.text = "Bonus coin minigame!"
				Global.specialMap = 1
			elif y == 7:
				label.text = "Steal minigame: Hold on to your coins!"
				#Global.specialMap = 2
			elif y == 8:
				label.text = "3 versus 1 minigame!"
				#Global.specialMap = 3
			labelShadow.text = label.text
			$Portal/Timer.start()

func _on_Timer_timeout():
	Global.fade.fadeOut("res://Scenes/Maps/Shop.tscn")
	pass