extends Node

onready var bigPanel = $Map/BigPanel
onready var CPUTimer = $Map/CPUTimer
onready var info = $Map/Info/Info1
onready var randomProfile = $Map/RandomPlayerProfile

var profileTurns = 0
var profiles
var winner

func _ready():
	Global.fade.fadeIn()
	
	if !Global.music.playing:
		Global.music.play()
	
	for x in 4:
		#Remove profiles of blank players
		if Global.players[x].type == 0:
			get_node("Map/CoinsProfileGameshow/CoinsProfile"+str(x+1)).queue_free()
	
	#Has a winner
	if Global.matchClass.lastWinner != null:
		winner = Global.matchClass.lastWinner
		chooseWinner()
		randomProfile.hide()
	#Generate a winner
	else:
		randomProfile.get_node("Timer").start()
		if Global.matchClass.currentTurn > 1:
			bigPanel.label.text = "Last match was a draw!\nChoosing a random player to play!"
		else:
			bigPanel.label.text = "Welcome to the gameshow!\nChoosing a random player to play!"
		bigPanel.labelShadow.text = bigPanel.label.text
	
	if Global.matchClass.type == 0: #Money/matches to win
		info.text = "First player to reach " + str(Global.matchClass.winMoney) + " wins!"
	elif Global.matchClass.type == 1: #Turns
		info.text = "Current turn: " + str(Global.matchClass.currentTurn) + "/" + str(Global.matchClass.turns)
	$Map/Info/Info2.text = info.text
	
	$Map/Objects/GoldenPig/AnimationPlayer.play("Standing",0.1,0.04)
	pass

func chooseWinner():
	$Map/Objects/Player.ready(winner)
	
	#If CPU controlled character, start timer for stop panels
	if Global.players[winner].type == 2:
		CPUTimer.wait_time = randi()%3+2
		CPUTimer.start()
	
	#Set color of text panel
	if Global.players[winner].team == 0:
		bigPanel.textureRect.modulate = Color(0.25, 0.25, 0.75, 1) #Blue
	elif Global.players[winner].team == 1:
		bigPanel.textureRect.modulate = Color(0.25, 0.75, 0.25, 1) #Green
	elif Global.players[winner].team == 2:
		bigPanel.textureRect.modulate = Color(0.75, 0.25, 0.25, 1) #Red
	elif Global.players[winner].team == 3:
		bigPanel.textureRect.modulate = Color(0.75, 0.75, 0.25, 1) #Yellow
	
	if Global.players[winner].type == 1:
		if Global.players[winner].deviceID == 5:
			bigPanel.label.text = "Player " + str(1+winner) + " is playing!\nPress        to stop!"
			bigPanel.get_node("Portal/Result/Space").show()
		else:
			bigPanel.label.text = "Player " + str(1+winner) + " is playing!\nPress     to stop!"
			bigPanel.get_node("Portal/Result/A").show()
	else:
		bigPanel.label.text = "Player " + str(1+winner) + " is playing!"
	bigPanel.labelShadow.text = bigPanel.label.text
	pass

func generateRandomWinner():
	var aux = winner
	winner = randi()%4
	if Global.players[winner].type == 0 || aux == winner:
		generateRandomWinner()
	else:
		var view = get_node("Map/CoinsProfileGameshow/CoinsProfile"+str(winner+1)+"/Viewport1")
		view.set_clear_mode(2)
		randomProfile.get_node("PlayerSprite").texture = view.get_texture()
		profileTurns += 1
		if profileTurns == 30:
			randomProfile.get_node("Timer").wait_time = 1.5
		randomProfile.get_node("Timer").start()

func _on_Timer_timeout():
	if profileTurns == 30:
		Global.matchClass.lastWinner = winner
		chooseWinner()
	else:
		generateRandomWinner()
	pass

func _physics_process(delta):
	#Change map
	if Global.devShortcuts && Input.is_action_just_pressed("ui_force_map_change"):
		Global.specialMap += 1
	
	#Determinate player
	if Global.matchClass.lastWinner == null:
		randomProfile.show()
		
	#Rotate panels
	else:
		randomProfile.hide()
		#If player controled character
		if Global.players[winner].type == 1:
			if Global.players[winner].deviceID == 5:
				if Input.is_action_just_pressed("ui_jump"):
					if bigPanel.turns == 0:
						bigPanel.turns = 1
						for x in 9:
							bigPanel.speed[x] = 30
					
					if bigPanel.turns == 3:
						bigPanel.turns = 4
						bigPanel.nextSpeed = 30
						
			#Movement with joypad
			elif Global.players[winner].deviceID == winner:
				if Input.is_action_just_pressed("joy_jump_"+str(winner)):
					if bigPanel.turns == 0:
						bigPanel.turns = 1
						for x in 9:
							bigPanel.speed[x] = 30
					
					if bigPanel.turns == 3:
						bigPanel.turns = 4
						bigPanel.nextSpeed = 30
					
		#If CPU controlled character
		elif Global.players[winner].type == 2:
			if CPUTimer.is_stopped():
				if bigPanel.turns == 0:
					bigPanel.turns = 1
					for x in 9:
						bigPanel.speed[x] = 30
					
					#Start new timer to stop again
					CPUTimer.wait_time = randi()%3+4
					CPUTimer.start()
				
				if bigPanel.turns == 3:
					bigPanel.turns = 4
					bigPanel.nextSpeed = 30
	pass