extends KinematicBody

onready var model = $MeshInstance

var item = preload("res://Scenes/Weapons/GroundItem.tscn")
var uranium = preload("res://Scenes/Toxic/Uranium.tscn")
var life = 1
var blinkingRate = 14
var knockbackdir
var parent
var gravity = -400
var velocity = Vector3(0,0,0)

func _physics_process(delta):
	if get_translation().y < -500:
		parent.numOfCrates -= 1
		queue_free()
	
	#Gravity
	velocity.y += gravity * delta
	velocity = move_and_slide(velocity, Vector3(0,1,0))
	pass

func takeDamage(attacker, damage,knockback):
	parent.numOfCrates -= 1
	self.queue_free()
	
	#Spawn uranium
	var r = randi()%3 + 3
	
	for x in r:
		#Instance uranium
		var w = item.instance()
		w.itemType = uranium.instance()
		w.set_translation(self.get_translation())
		parent.add_child(w)
	pass

func _on_Area_body_shape_entered(body_id, body, body_shape, area_shape):
	if body.is_in_group("Player") || body.is_in_group("Crate"):
		takeDamage(0,0,0)
	pass