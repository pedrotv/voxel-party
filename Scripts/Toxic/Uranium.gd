extends RigidBody

onready var timer = $Timer

var force = 50

func _ready():
	self.apply_impulse(Vector3(0,0,0),Vector3(randi()%2 - 1,3,randi()%2 - 1) * force)
	pass

func _physics_process(delta):
	if get_translation().y < -500:
		queue_free()

func _on_Area_body_shape_entered(body_id, body, body_shape, area_shape):
	if (body.is_in_group("Player")):
		if timer.is_stopped():
			body.collectables +=1
			self.queue_free()
	pass