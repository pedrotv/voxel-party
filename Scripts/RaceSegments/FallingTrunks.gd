extends Spatial

var trunks = []
var fall = []
var restore = []

onready var shakeTimer = $ShakeTimer
onready var fallTimer = $FallTimer

func _ready():
	trunks.resize(12)
	for x in 12:
		trunks[x] = get_node("FallTrunk" + str(x+1))
	
	restore.resize(4)
	fall.resize(4)
	for x in 4:
		generateFloor(x)
	
	shakeTimer.start()
	pass

func _physics_process(delta):
	if !shakeTimer.is_stopped():
		for x in 4:
			fall[x].get_node("AnimationPlayer").play("Shake",0,5)
	
	pass

func generateFloor(x):
	var r = randi()%trunks.size()
	fall[x] = trunks[r]
	trunks.remove(r)
	pass

func _on_ShakeTimer_timeout():
	for x in 4:
		if restore[x] != null:
			restore[x].get_node("AnimationPlayer").play("Restore",0,2)
			trunks.append(restore[x])
	
	for x in 4:
		fall[x].get_node("AnimationPlayer").play("Fall",0,2)
	
	fallTimer.start()
	pass

func _on_FallTimer_timeout():
	for x in 4:
		if restore[x] != null:
			restore[x].get_node("AnimationPlayer").stop()
		
		fall[x].get_node("AnimationPlayer").stop()
		restore[x] = fall[x]
		
		generateFloor(x)
	
	shakeTimer.start()
	pass