extends Spatial

func _ready():
	var a = randi()%2
	self.rotation_degrees.y = a*180

func _on_Timer_timeout():
	for x in 3:
		get_node("Log"+str(x+1)+"/AnimationPlayer").play("Rotate",0,0.5)
	pass

func _on_AnimationPlayer1_animation_finished(anim_name):
	if anim_name == "Rotate":
		get_node("Log1/AnimationPlayer").stop()
	pass

func _on_AnimationPlayer2_animation_finished(anim_name):
	if anim_name == "Rotate":
		get_node("Log2/AnimationPlayer").stop()
	pass

func _on_AnimationPlayer3_animation_finished(anim_name):
	if anim_name == "Rotate":
		get_node("Log3/AnimationPlayer").stop()
	pass