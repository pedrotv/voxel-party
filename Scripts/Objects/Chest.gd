extends KinematicBody

var item = preload("res://Scenes/Weapons/GroundItem.tscn")
var coin = preload("res://Scenes/Objects/Coin.tscn")

var gravity = -400
var velocity = Vector3(0,0,0)
var open = false
var turns = 10

func _physics_process(delta):
	if get_translation().y < -500:
		queue_free()
	
	#Gravity
	velocity.y += gravity * delta
	velocity = move_and_slide(velocity, Vector3(0,1,0))
	
	pass

func takeDamage(attacker, damage, knockbackForce):
	if !open:
		open = true
		
		$AnimationPlayer.play("Open",0,3)
		
		$Timer.start()
		
	pass

func _on_Timer_timeout():
	for x in 3:
		var w = item.instance()
		w.itemType = coin.instance()
		var t = self.translation
		w.set_translation(Vector3(t.x,t.y+10,t.z))
		w.get_node("CollisionShape").shape = w.get_node("CollisionShape").shape.duplicate()
		w.get_node("CollisionShape").shape.radius = 9
		get_parent().add_child(w)
	turns -= 1
	if turns == 0:
		self.queue_free()
	pass