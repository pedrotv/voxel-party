extends KinematicBody

onready var timer = $Timer

var speed = 1

var rot_speed = rad2deg(0.003)
var dir = 1

func _physics_process(delta):
	if speed < 1:
		speed = 1
	pass

func takeDamage(attacker, damage, knockbackForce):
	if timer.is_stopped():
		timer.start()
		speed += 2 #Increase speed of the map spinning
		
	pass