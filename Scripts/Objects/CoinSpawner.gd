extends Spatial

var item = preload("res://Scenes/Weapons/GroundItem.tscn")
var coin = preload("res://Scenes/Objects/Coin.tscn")
var sackOfCoins = preload("res://Scenes/Objects/SackOfCoins.tscn")
var chest = preload("res://Scenes/Crates/Chest.tscn")

var coinNum = 1
var isChest = false

func _on_Timer_timeout():
	for x in coinNum:
		var w = item.instance()
		if coinNum > 1: #If it's spawning more than one coin
			var r = randi()%100
			if r < 3: #3% chance of getting a chest
				w.queue_free()
				isChest = true
			elif r < 13: #10% chance of getting a sack of coins
				w.itemType = sackOfCoins.instance()
			else: #87% chance of getting a coin
				w.itemType = coin.instance()
		else: #Spawn one coin
			w.itemType = coin.instance()
		
		if isChest:
			w = chest.instance()
			isChest = false
		else:
			w.get_node("CollisionShape").shape = w.get_node("CollisionShape").shape.duplicate()
			w.get_node("CollisionShape").shape.radius = 9
		
		w.set_translation(Vector3(randi()%400-200,225,randi()%400-200))
		get_parent().add_child(w)
	pass