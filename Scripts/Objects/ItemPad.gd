extends KinematicBody

var camera

onready var itemPad = $ItemPad

func _ready():
	camera = get_node("/root/Shop/MapSetup/Camera")
	itemPad.hide()

func _physics_process(delta):
	#itemPad.look_at(camera.translation, Vector3(0,1,0))
	
	var areaCollsion = $Area.get_overlapping_bodies()
	if areaCollsion.size() > 0:
		for N in areaCollsion.size():
			if areaCollsion[N].is_in_group("Player"):
				itemPad.show()
	else:
		itemPad.hide()
	pass
