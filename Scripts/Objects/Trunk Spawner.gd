extends Spatial

onready var timer = $SpawnTimer

export (int) var dir 

var trunk = load("res://Scenes/RaceSegments/FloatingBox.tscn")

func _on_SpawnTimer_timeout():
	var t = trunk.instance()
	t.dir = dir
	t.spawned = true
	self.add_child(t)
	timer.wait_time = randi()%6 + 4 
	timer.start()
	pass
