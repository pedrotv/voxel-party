extends Area

export (bool) var kill

func _on_StopTrunk_body_shape_entered(body_id, body, body_shape, area_shape):
	if (body.is_in_group("Ground")):
		if kill:
			body.queue_free()
		else:
			body.velocity = Vector3(0,0,0)
			if body.dir == 1:
				body.dir = -1
			else:
				body.dir = 1
	pass
