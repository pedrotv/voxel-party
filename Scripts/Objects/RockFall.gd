extends KinematicBody

onready var timer = $FallTimer

var speed = 500
var velocity = Vector3()
var falling = false
var initialY

func _ready():
	initialY = self.get_translation().y
	velocity = Vector3(0,0,0)
	self.rotation_degrees.y = randi()%360
	pass

func _physics_process(delta):
	velocity = velocity.normalized() * speed * delta
	if falling:
		velocity.y += -1
		if timer.is_stopped():
			timer.start()
	
	if !timer.is_stopped():
		if timer.time_left < 1:
			$CollisionShape.disabled = true
	
	move_and_slide(velocity, Vector3(0,1,0))
	
	pass

func _on_FallTimer_timeout():
	falling = false
	velocity.y = 0
	$CollisionShape.disabled = false
	$Area/CollisionShape.disabled = false
	self.set_translation(Vector3(self.get_translation().x, initialY, self.get_translation().z))
	self.rotation_degrees.y = randi()%360
	
	pass

func _on_Area_body_shape_entered(body_id, body, body_shape, area_shape):
	if body.is_in_group("Player"):
		falling = true
		$Area/CollisionShape.disabled = true
	pass