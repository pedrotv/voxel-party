extends Area

var active = false

export (int) var id

func _on_Checkpoint_body_shape_entered(body_id, body, body_shape, area_shape):
	if (body.is_in_group("Player")) && body.checkpoint < id:
		body.checkpoint = id
		body.initPosition.z = self.get_global_transform().origin.z
		active = true
	pass