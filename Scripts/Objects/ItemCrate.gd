extends KinematicBody

onready var model = $MeshInstance

var item = preload("res://Scenes/Weapons/GroundItem.tscn")
var brokenCratePieces = preload("res://Scenes/Crates/BrokenItemCratePieces.tscn")
var parent
var life = 1
var blinkingRate = 14
var knockbackdir

var gravity = -400
var velocity = Vector3(0,0,0)

func _ready():
	parent = get_parent()
	var aux = model
	aux.set_mesh(aux.get_mesh().duplicate())
	var dupe = aux.get_mesh().surface_get_material(0).duplicate()
	aux.get_mesh().surface_set_material(0, dupe)
	model = aux

func _physics_process(delta):
	if $DeleteTimer.time_left < 3:
		blinkEffect()
	
	if get_translation().y < -500:
		queue_free()
	
	#Gravity
	velocity.y += gravity * delta
	velocity = move_and_slide(velocity, Vector3(0,1,0))
	
	pass

#Blink effect
func blinkEffect():
	if Engine.get_frames_drawn() % blinkingRate < blinkingRate/2:
		model.get_mesh().surface_get_material(0).albedo_color = Color(0.5,0,0,1) 
	else:
		model.get_mesh().surface_get_material(0).albedo_color = Color(1,1,1,1)
	pass

func takeDamage(attacker, damage, knockbackForce):
	#Instanciate broken pieces
	var b = brokenCratePieces.instance()
	parent.add_child(b)
	b.set_translation(self.get_translation())
	
	#Instance item
	var w = item.instance()
	w.id = randi()%Global.weapons.size()
	w.itemType = Global.weapons[w.id].instance()
	w.set_translation(self.get_translation())
	parent.add_child(w)
	
	self.queue_free()
	pass

func _on_DeleteTimer_timeout():
	model.get_mesh().surface_get_material(0).albedo_color = Color(1,1,1,1)
	queue_free()
	pass

func _on_Area_body_shape_entered(body_id, body, body_shape, area_shape):
	if body.is_in_group("Player") || body.is_in_group("Crate"):
		takeDamage(0,0,0)
	pass