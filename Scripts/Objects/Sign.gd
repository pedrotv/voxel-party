extends KinematicBody

onready var anim = $AnimationPlayer

var left = true
var dir = 1

func takeDamage(attacker, damage, knockbackForce):
	if !anim.is_playing():
		if left:
			dir = -1
			anim.play("GoRight")
			left = false
		else:
			dir = 1
			anim.play("GoLeft")
			left = true
		pass

func _on_AnimationPlayer_animation_finished(anim_name):
	anim.stop()
	pass