extends KinematicBody

var speed = 1500
var velocity = Vector3()

var dir
var spawned = false

func _ready():
	if !spawned:
		dir = randi()%2
		if dir == 0:
			dir = -1
		self.translation.x = randi()%180 - 90
		speed += randi()%4 * 125
	
	velocity = Vector3(0,0,0)
	
	pass

func _physics_process(delta):
	velocity = velocity.normalized() * speed * delta
	velocity.x += dir
	
	move_and_slide(velocity, Vector3(0,1,0))#, false, 4, 0.785398, false)
	
	pass
