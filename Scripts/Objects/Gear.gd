extends Spatial

var rot_speed = rad2deg(0.01)
var speed = []

func _ready():
	for x in 3:
		speed.append(randi()%3+1)

func _physics_process(delta):
	var aux = $Gear.get_rotation()
	$Gear.set_rotation(Vector3(aux.x, aux.y, aux.z + (-rot_speed * speed[0]) * delta))
	
	var aux2 = $Gear2.get_rotation()
	$Gear2.set_rotation(Vector3(aux2.x, aux2.y, aux2.z + (rot_speed * speed[1]) * delta))
	
	var aux3 = $Drill.get_rotation()
	$Drill.set_rotation(Vector3(aux3.x, aux3.y, aux3.z + (rot_speed * speed[2]) * delta)) 