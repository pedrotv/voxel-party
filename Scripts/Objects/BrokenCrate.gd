extends RigidBody

var blinkingRate = 20

onready var model = $MeshInstance

func _ready():
	self.apply_impulse(Vector3(0,0,0),Vector3(randi()%20 - 10,0,randi()%20 - 10) * 500)

func _on_Timer_timeout():
	self.queue_free()
	pass
