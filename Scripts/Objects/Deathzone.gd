extends KinematicBody

export (bool) var lava = false
export (bool) var water = false
export (bool) var acid = false
export (bool) var loop = false

export (bool) var rise = false

# -22 12
var goingUp = true
var speed = 5
var velocity = Vector3(0,0,0)

var timeDown = 8.5
var timeUp = 3.5

func _ready():
	if rise:
		$Arrows.hide()
		rise = false
		$Timer.wait_time = timeDown
		$Timer.start()

func _physics_process(delta):
	if rise:
		#Make deathzone go up
		if goingUp:
			velocity.y += speed * delta
			velocity = move_and_slide(velocity, Vector3(0,1,0))
			#Make it stop
			if self.translation.y >= 14:
				velocity = Vector3(0,0,0)
				rise = false
				goingUp = false
				$Timer.wait_time = timeUp
				$Timer.start()
		else:
			#Make deathzone go down
			velocity.y -= speed * delta
			velocity = move_and_slide(velocity, Vector3(0,1,0))
			#Make is stop
			if self.translation.y <= -12:
				velocity = Vector3(0,0,0)
				rise = false
				goingUp = true
				$Timer.wait_time = timeDown
				$Timer.start()
	else:
		if has_node("Arrows"):
			if $Timer.time_left < 1.0:
				$Arrows.show()
				if goingUp:
					$Arrows/AnimationPlayer.play("Up",0,3)
				else:
					$Arrows/AnimationPlayer.play("Down",0,3)
			else:
				$Arrows.hide()
	pass

func _on_Area_body_shape_entered(body_id, body, body_shape, area_shape):
	if body.is_in_group("Player"):
		body.inDeathzone = true
		body.inLava = lava
		body.inWater = water
		body.inAcid = acid
		body.inLoop = loop
	pass

func _on_Area_body_shape_exited(body_id, body, body_shape, area_shape):
	if body.is_in_group("Player"):
		body.inDeathzone = false
		body.inAcid = false
	pass

func _on_Timer_timeout():
	#Start rising again
	rise = true
	pass
