extends Spatial

onready var raycast = $RayCast
onready var spotlightCenter = $SpotlightCenter/SpotLight

func _physics_process(delta):
	if raycast.is_colliding():
		spotlightCenter.translation.y = raycast.get_collision_point().y
	pass
