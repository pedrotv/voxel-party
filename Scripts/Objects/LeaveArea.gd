extends Area

var leavingPlayers = 0

func _on_LeaveArea_body_shape_entered(body_id, body, body_shape, area_shape):
	if body.is_in_group("Player"):
		body.canMove = false
		body.autoWalkForward = true
		leavingPlayers += 1
	pass

func _physics_process(delta):
	if get_node("../ChangeSceneTimer").is_stopped() && leavingPlayers >= Global.matchClass.numPlayers:
		get_node("../MatchTimer").stop()
		get_node("../ChangeSceneTimer").wait_time = 1
		get_node("../ChangeSceneTimer").start()