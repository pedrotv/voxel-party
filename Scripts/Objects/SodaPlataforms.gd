extends KinematicBody

var blinkingRate = 20
var state = 0
var speed = 1000
var initPosition = Vector3()
var models = []
var platNumber = 6

onready var model = $Model

onready var countdownTimer = $CountDownTimer
onready var sinkTimer = $GoDownTimer
onready var restoreTimer = $RestoreTimer

func _ready():
	models = Global.purpleSeaPlataforms
	initPosition = self.translation

func _physics_process(delta):
	var velocity = Vector3(0,0,0)
	
	model.set_mesh(Global.purpleSeaPlataforms[platNumber])
	
	#Plataform sinking
	if state == 2:
		#Go down
		velocity.y -= speed * delta
		move_and_slide(velocity, Vector3(0,1,0))
	#Plataforming restoring
	elif state == 3:
		#Go up
		if translation.y < initPosition.y:
			velocity.y += speed * delta
			move_and_slide(velocity, Vector3(0,1,0))
		else:
			translation = initPosition
			state = 0
	
	#If a player steps on it
	var areaCollsion = $Area.get_overlapping_bodies()
	if areaCollsion.size() > 0:
		for N in areaCollsion.size():
			if areaCollsion[N].is_in_group("Player") || areaCollsion[N].is_in_group("Crate"):
				if state == 0:
					countdownTimer.start()
					state = 1
					platNumber -= 1
	pass

#Countdown for the plataform to start falling
func _on_CountDownTimer_timeout():
	platNumber -= 1
	
	if platNumber > 0:
		countdownTimer.start()
	else:
		state = 2
		sinkTimer.start()
	pass

#Start sinking
func _on_GoDownTimer_timeout():
	restoreTimer.start()
	pass

#Make plataform bigger
func _on_RestoreTimer_timeout():
	state = 3
	platNumber = 6
	pass
