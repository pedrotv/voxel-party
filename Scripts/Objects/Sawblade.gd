extends KinematicBody

var brokenSaw = preload("res://Scenes/Objects/BrokenSaw.tscn")

var dir = 150

onready var punchCol = $CollisionShape

func _ready():
	$AnimationPlayer.play("Move",0,0.25)
	pass 

func _physics_process(delta):
	var m = self.get_rotation()
	self.set_rotation(Vector3(0, m.y + dir * delta, 0))
	pass

func breakSaw():
	var b = brokenSaw.instance()
	get_node("../../Map").add_child(b)
	b.set_translation(self.get_translation())
	queue_free()
	pass

func _on_Area_body_shape_entered(body_id, body, body_shape, area_shape):
	#if body.is_in_group("Crate"):
	#	body.takeDamage(self,8,8)
	if body.is_in_group("Player"):
		if body.isHuge:
			 breakSaw()
		else:
			body.takeDamage(self,8,8)
	pass