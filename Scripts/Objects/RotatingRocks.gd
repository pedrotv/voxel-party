extends Spatial

var r 

export (int) var dir = -1
export (int) var speed = 1

func _ready():
	self.rotation_degrees.y = randi()%360
	r = 2.5#randi()%3 + 1
	pass

func _physics_process(delta):
	var rot_speed = rad2deg(0.003) * speed
	var s = self.get_rotation()
	self.set_rotation(Vector3(s.x,s.y + (dir*rot_speed*r) * delta,s.z))
	if has_node("RockCenter"):
		$Rock1.set_rotation(Vector3(s.x,s.y + (-dir*rot_speed*r) * delta,s.z))
		$Rock2.set_rotation(Vector3(s.x,s.y + (-dir*rot_speed*r) * delta,s.z))
		$Rock3.set_rotation(Vector3(s.x,s.y + (-dir*rot_speed*r) * delta,s.z))
		$Rock4.set_rotation(Vector3(s.x,s.y + (-dir*rot_speed*r) * delta,s.z))
		$RockCenter.set_rotation(Vector3(s.x,s.y + (-dir*rot_speed*r) * delta,s.z))
	pass