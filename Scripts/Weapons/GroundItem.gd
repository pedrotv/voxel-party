extends KinematicBody

onready var item = $Item
onready var deleteTimer = $DeleteTimer

var g = -300

var velocity = Vector3(0,0,0)
var itemType
var id = -1
var aux = null

var dir = Vector3(0,0,0)
var speed = 1500
var jumpHeight = 75

var active = true

export (bool) var shop = false
export (int) var shopItem = 0
export (int) var shopPrice = 250

func _ready():
	if shop:
		id = shopItem
		itemType = Global.weapons[id].instance()
	
	itemType.start = true
	item.add_child(itemType)
	
	#Instance the other boot
	if itemType.typeName == "Boot":
		aux = Global.weapons[id].instance()
		aux.start = true
		item.add_child(aux)
		itemType.get_node("MeshInstance").translation.x -= 2.5
		aux.get_node("MeshInstance").translation.x += 2.5
	
	if itemType.typeName != "Uranium" && itemType.typeName != "Bomb" && !shop:
		$DeleteTimer.start() #Start timer for deletion (excludes bomb and uranium)
	
	#Gives a random speed so it goes in a random direction
	if !shop:
		speed = 1000 + randi()%500
		velocity.y += jumpHeight
		dir = Vector3(randi()%2 - 1 + randf(), 0, randi()%2 - 1 + randf())
	pass

func _physics_process(delta):
	#If isn't touching the floor and is uranium, moves in a random direction
	if !is_on_floor(): 
		dir = dir.normalized() * speed * delta
		velocity.x = dir.x
		velocity.z = dir.z
	#stops movement
	else:
		velocity.x = 0
		velocity.z = 0
	
	#If gravity is active, starts falling
	if active:
		velocity.y += g * delta
		velocity = move_and_slide(velocity, Vector3(0,1,0))
	#Stands still
	else:
		velocity.y = 0
		velocity = move_and_slide(velocity, Vector3(0,1,0))
	
	#Deletes if it's too far down
	if get_translation().y < -500:
		queue_free()
	
	#Starts blinking when about to get deleted
	if deleteTimer.time_left < 3 && !deleteTimer.is_stopped():
		itemType.blinkEffect()
		if aux != null: #Second boot blink
			aux.blinkEffect()
	pass

#Destroy object (fix color first)
func _on_DeleteTimer_timeout():
	itemType.resetColor()
	if aux != null:
		aux.resetColor()
	queue_free()
	pass