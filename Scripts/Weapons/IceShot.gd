extends KinematicBody

var player
var speed = 15000
var direction = Vector3(0,0,0)
var rot

func _ready():
	rot = player.rotation_degrees.y
	pass

func _physics_process(delta):
	direction = Vector3(1, 0, 0).rotated(Vector3(0, 1, 0).normalized(),deg2rad(rot-90)) * speed * delta
	direction = move_and_slide(direction, Vector3(0,1,0))
	pass

func _on_Timer_timeout():
	queue_free()
	pass

func _on_Area_body_shape_entered(body_id, body, body_shape, area_shape):
	if body.is_in_group("Player") && body != player:
		body.isFrozen = true
		self.queue_free()
	if body.is_in_group("Crate"):
		body.takeDamage(self, 1, 0)
		self.queue_free()
	pass