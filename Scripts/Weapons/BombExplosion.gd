extends Area

onready var punchCol = $punchCol

func _ready():
	$Particles.emitting = true

func _on_BombExplosion_body_shape_entered(body_id, body, body_shape, area_shape):
	if body.is_in_group("Player") || body.is_in_group("Crate"):
		body.takeDamage(self,8,8)
	pass

func _on_Timer_timeout():
	queue_free()
	pass