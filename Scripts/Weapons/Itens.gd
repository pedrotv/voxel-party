extends Spatial

onready var itemCrate = preload("res://Scenes/Crates/ItemCrate.tscn")

export (Vector2) var borders 

export (bool) var start = true

func _on_Timer_timeout():
	if Global.itens:
		var i = itemCrate.instance()
		var x = randi()%int(borders.x) - int(borders.x/2)
		var z = randi()%int(borders.y) - int(borders.y/2)
		i.set_translation(Vector3(x,i.translation.y,z))
		add_child(i)