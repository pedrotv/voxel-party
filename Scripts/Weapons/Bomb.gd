extends Spatial

var tex = load("res://Models/ObjType/Weapons/Bomb.png")
var tex2 = load("res://Models/ObjType/Weapons/Bomb2.png")
var particles = load("res://Scenes/Weapons/BombParticles.tscn")

export (String) var boneName
export (String) var animName
export (String) var typeName = "Weapon" #Weapon, powerup, boot
export (Vector3) var fixPos
export (Vector3) var fixRot
export (int) var damage
export (int) var knockbackForce

onready var model = $MeshInstance 
onready var blowUpTimer = $BlowUpTimer

var isHuge = true
var timer
var start = false
var dir = 0.1
var blinkingRate = 14
var player
var groundItem
var bomb
var grabbed = false

func _ready():
	model.set_mesh(model.get_mesh().duplicate())
	var dupe = model.get_mesh().surface_get_material(0).duplicate()
	model.get_mesh().surface_set_material(0, dupe)
	model.mesh.surface_get_material(0).set_texture(0,tex)
	groundItem = get_parent().get_parent()
	
	#Sets up timer for wiggle animation
	if start:
		timer = Timer.new()
		add_child(timer)
		timer.connect("timeout",self,"_on_Timer_timeout")
		timer.one_shot = true
		timer.start()
	#Fix translation/rotation on chraracter
	else:
		var m = $MeshInstance
		m.set_translation(m.get_translation() + fixPos)
		m.set_rotation_degrees(m.get_rotation_degrees() + fixRot)

func _physics_process(delta):
	start = false
	
	#Rotates within it's Y axis
	if start:
		var m = self.get_rotation()
		self.set_rotation(Vector3(m.x + dir * delta, m.y + 1 * delta, m.z + dir * delta))
	#No rotation
	else:
		self.set_rotation_degrees(Vector3(0,0,0))
	
	if grabbed && groundItem.is_on_floor() && blowUpTimer.time_left > 0.5:
		#print("blow up")
		blowUpTimer.stop()
		blowUpTimer.wait_time = 0.5
		blowUpTimer.start()
	
	#Blink to blow up
	if blowUpTimer.time_left <= 2:
		if Engine.get_frames_drawn() % blinkingRate < blinkingRate/2:
			model.get_mesh().surface_get_material(0).set_texture(0,tex)
		else:
			model.get_mesh().surface_get_material(0).set_texture(0,tex2)
	if blowUpTimer.time_left <= 0.5 && $MeshInstance.visible:
		$BombExplosion/CollisionShape.disabled = false
		$MeshInstance.hide()
		$BombExplosion/MeshInstance2.show()
#		var p = particles.instance()
#		get_parent().get_parent().get_parent().add_child(p)
#		p.global_transform = get_parent().get_parent().global_transform
#		p.emitting = true

func _on_BombExplosion_body_shape_entered(body_id, body, body_shape, area_shape):
	if body.is_in_group("Player") || body.is_in_group("Crate"):
		body.takeDamage(self,8,8)
	pass

func _on_Timer_timeout():
	if dir == 0.1:
		dir = -0.1
	else:
		dir = 0.1
	timer.wait_time = 2
	timer.start()
	pass

func blinkEffect():
	pass

func resetColor():
	pass

func weaponEffect(map,player):
	var pos = groundItem.get_global_transform()
	
	groundItem.get_parent().remove_child(groundItem)
	player.get_parent().add_child(groundItem)
	
	groundItem.set_global_transform(pos)
	
	#start = true #Start rotaion
	groundItem.active = true #Start gravity
	groundItem.velocity.y += groundItem.jumpHeight #Make item jump
	groundItem.dir = Vector3(player.dir.x, 0, player.dir.z) #Throw
	groundItem.speed = 5000
	groundItem.get_node("CollisionShape").disabled = false
	grabbed = true
	
	pass

func _on_BlowUpTimer_timeout():
	#Delete
	get_parent().get_parent().queue_free()
	pass

func follow(b,p):
	player = p
	var t = player.skeleton.get_bone_global_pose(player.skeleton.find_bone(boneName))
	b.get_node("CollisionShape").disabled = true
	b.set_transform(t)
	b.set_rotation_degrees(Vector3(0,0,0))
	b.set_translation(Vector3(0,20,0))