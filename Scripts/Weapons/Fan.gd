extends "res://Scripts/Weapons/BaseWeapon.gd"

var iceShot = load("res://Scenes/Weapons/AirShot.tscn")

onready var effectTimer = $EffectTimer

func weaponEffect(map,player):
	if effectTimer.is_stopped():
		effectTimer.start()
		var i = iceShot.instance()
		i.player = player
		map.add_child(i)
		i.set_global_transform(self.get_global_transform())