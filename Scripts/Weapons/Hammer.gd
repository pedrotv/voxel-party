extends "res://Scripts/Weapons/BaseWeapon.gd"

onready var punchCol = $MeshInstance/Area/CollisionShape
onready var effectTimer = $EffectTimer

func _on_Area_body_shape_entered(body_id, body, body_shape, area_shape):
	if !start:
		if body.is_in_group("Player") && body.numID != player.numID || body.is_in_group("Crate") || body.is_in_group("InteractableObj"):
			body.takeDamage(player,damage+1,knockbackForce+10)
	pass

func weaponEffect(map,player):
	if has_node("Sound"):
		Global.sfx3DSound(get_node("Sound"))
		get_node("Sound").play()
	
	player.punchCol.disabled = true
	punchCol.disabled = false
	if effectTimer.is_stopped():
		effectTimer.start()
	pass

func _on_EffectTimer_timeout():
	punchCol.disabled = true
	pass