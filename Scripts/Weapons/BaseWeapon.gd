extends Spatial

export (String) var boneName
export (String) var animName
export (String) var typeName = "Weapon" #Weapon, powerup, boot
export (Vector3) var fixPos
export (Vector3) var fixRot
export (int) var damage
export (int) var knockbackForce

onready var model = $MeshInstance 

var uraniumShader = load("res://Shaders/Uranium.tres")

var timer
var start = false
var dir = 0.1
var blinkingRate = 14
var player

var defaultTransform = null

func _ready():
	if typeName != "Uranium":
		var aux = model
		aux.set_mesh(aux.get_mesh().duplicate())
		var dupe = aux.get_mesh().surface_get_material(0).duplicate()
		aux.get_mesh().surface_set_material(0, dupe)
		model = aux
	
	if start:  
		timer = Timer.new()
		add_child(timer)
		timer.connect("timeout",self,"_on_Timer_timeout")
		timer.one_shot = true
		timer.start()
	else:
		var m = $MeshInstance
		m.set_translation(m.get_translation() + fixPos)
		m.set_rotation_degrees(m.get_rotation_degrees() + fixRot)

func _physics_process(delta):
	if start:
		var m = self.get_rotation()
		self.set_rotation(Vector3(m.x + dir * delta, m.y + 1 * delta, m.z + dir * delta))

func _on_Timer_timeout():
	if dir == 0.1:
		dir = -0.1
	else:
		dir = 0.1
	timer.wait_time = 2
	timer.start()
	pass

#Blink effect
func blinkEffect():
	if Engine.get_frames_drawn() % blinkingRate < blinkingRate/2:
		model.get_mesh().surface_get_material(0).albedo_color = Color(0.5,0,0,1) 
	else:
		model.get_mesh().surface_get_material(0).albedo_color = Color(1,1,1,1)
	pass

func resetColor():
	model.get_mesh().surface_get_material(0).albedo_color = Color(1,1,1,1)

func weaponEffect(map,player):
	if has_node("Sound"):
		Global.sfx3DSound(get_node("Sound"))
		get_node("Sound").play()
	pass

func follow(p):
	player = p
	
	#Make item follow
	var t = player.skeleton.get_bone_global_pose(player.skeleton.find_bone(boneName))
	set_transform(t)
	