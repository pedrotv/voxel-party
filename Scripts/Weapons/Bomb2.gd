extends RigidBody

var tex = load("res://Models/ObjType/Weapons/Bomb.png")
var tex2 = load("res://Models/ObjType/Weapons/Bomb2.png")
var explosion = load("res://Scenes/Weapons/BombExplosion.tscn")

var typeName = "Bomb"
var blinkingRate = 14
var parent
var explosionEffect

onready var model = $MeshInstance
onready var punchCol = $punchCol
onready var cs = $Area/CS
onready var timer = $Timer

func _ready():
	model.set_mesh(model.get_mesh().duplicate())
	var dupe = model.get_mesh().surface_get_material(0).duplicate()
	model.get_mesh().surface_set_material(0, dupe)
	model.mesh.surface_get_material(0).set_texture(0,tex)

func _physics_process(delta):
	#Blink
	if timer.time_left <= 1:
		if Engine.get_frames_drawn() % blinkingRate < blinkingRate/2:
			model.get_mesh().surface_get_material(0).set_texture(0,tex)
		else:
			model.get_mesh().surface_get_material(0).set_texture(0,tex2)

func _on_Timer_timeout():
	#Instance explosion
	explosionEffect = explosion.instance()
	parent.add_child(explosionEffect)
	explosionEffect.set_translation(self.get_translation())
	#Delete
	queue_free()
	pass