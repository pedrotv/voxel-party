extends "res://Scripts/Weapons/BaseWeapon.gd"

var ammo = 8
var shot = false
var m
var p

var iceShot = preload("res://Scenes/Weapons/IceShot.tscn")

onready var effectTimer = $EffectTimer

func _physics_process(delta):
	if ammo < 2:
		blinkEffect()

func weaponEffect(map,player):
	if has_node("Sound"):
		Global.sfx3DSound(get_node("Sound"))
		get_node("Sound").play()
	
	if effectTimer.is_stopped():
		shot = false
		effectTimer.start()
		m = map
		p = player
	
	if effectTimer.time_left <= 0.3:
		if !shot:
			shot = true
			spawnShot()
			ammo -= 1
			if ammo <= 0:
				self.queue_free()
				player.weapon = null
	pass

func spawnShot():
	var i = iceShot.instance()
	i.player = p
	m.add_child(i)
	i.set_global_transform(player.punchCol.get_global_transform())