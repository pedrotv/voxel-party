extends Spatial

onready var itemCrate = preload("res://Scenes/Crates/ItemCrate.tscn")
onready var toxicContainer = preload("res://Scenes/Toxic/ToxicContainer.tscn")

var points = []
var numOfCrates = 0
var maxCrates = 5

export (bool) var start = true
export (bool) var spawnsContainers = false
export (float) var frequency = 5

func _ready():
	$Timer.wait_time = frequency
	
	if start:
		$Timer.start()
	
	var aux = 0
	points.resize(self.get_child_count()-1)
	for x in self.get_children():
		if x.name != "Timer":
			points[aux] = x
			aux += 1
	pass

func _physics_process(delta):
	if has_node("Checkpoint") && get_node("Checkpoint").active:
		start = true
	
	if $Timer.is_stopped():
		if start:
			$Timer.start()

func _on_Timer_timeout():
	if spawnsContainers:
		for aux in 2:
			if numOfCrates < maxCrates:
				#Spawn Toxic Container
				var t = toxicContainer.instance()
				var r = randi()%points.size()
				var x = points[r].translation.x
				var z = points[r].translation.z
				t.set_translation(Vector3(x,0,z))
				add_child(t)
				t.parent = self
				numOfCrates += 1
	
	if Global.itens:
		#Spawn item crate
		var i = itemCrate.instance()
		var r2 = randi()%points.size()
		var x = points[r2].translation.x
		var z = points[r2].translation.z
		i.set_translation(Vector3(x,0,z))
		add_child(i)
		i.parent = self