extends KinematicBody

var player
var speed = 10000
var direction = Vector3(0,0,0)
var rot

func _ready():
	rot = player.rotation_degrees.y
	pass

func _physics_process(delta):
	direction = Vector3(1, 0, 0).rotated(Vector3(0, 1, 0).normalized(),deg2rad(rot-90)) * speed * delta
	direction = move_and_slide(direction, Vector3(0,1,0))
	pass

func _on_Timer_timeout():
	queue_free()
	pass